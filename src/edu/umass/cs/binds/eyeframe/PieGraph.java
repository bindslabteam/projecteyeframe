package edu.umass.cs.binds.eyeframe;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;

import javafx.scene.paint.Color;

import java.util.ArrayList;

class PieGraph extends JComponent {
    private static Map<Integer, Integer> cursors =
        new HashMap<Integer, Integer>();
    {
        cursors.put(1, Cursor.N_RESIZE_CURSOR);
        cursors.put(2, Cursor.W_RESIZE_CURSOR);
        cursors.put(4, Cursor.S_RESIZE_CURSOR);
        cursors.put(8, Cursor.E_RESIZE_CURSOR);
        cursors.put(3, Cursor.NW_RESIZE_CURSOR);
        cursors.put(9, Cursor.NE_RESIZE_CURSOR);
        cursors.put(6, Cursor.SW_RESIZE_CURSOR);
        cursors.put(12, Cursor.SE_RESIZE_CURSOR);
    }

    protected static final int NORTH = 1;
    protected static final int WEST = 2;
    protected static final int SOUTH = 4;
    protected static final int EAST = 8;

    protected ArrayList<PieGraph.Slice> slices;
    protected double[] seperatorLinesAngles;
    // 0 stands for origin, the displacement from the center of the planes
    // (the display)
    // this is the position of the center of the pie graph from the origin of
    // the display
    protected int x0;
    protected int y0;
    protected double startAngle = 0.0;
    protected double stopAngle = 360.0;

    private int dragInset;
    private double minimumAngleSize;
    private PieGraphMouseAdapter mouseAdapter;

    private double totalValue;

    /**
     * @wbp.parser.constructor
     */
    PieGraph() {
        this(new ArrayList<PieGraph.Slice>(),5,2.0);
    }

    PieGraph(ArrayList<PieGraph.Slice> slices) {
        this(slices,5,2.0);
    }

    PieGraph(ArrayList<PieGraph.Slice> slices, int dragInset,
            double minimumAngleSize) {
        setSlices(slices);
        initSeperatorLinesAngles();
        setDragInset(dragInset);
        this.minimumAngleSize = minimumAngleSize;
        mouseAdapter = new PieGraphMouseAdapter(this);
        this.addMouseListener(mouseAdapter);
        this.addMouseMotionListener(mouseAdapter);
        this.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                int width = getWidth();
                setSize(width,width);
            }
            public void componentMoved(ComponentEvent e) {
            }
            public void componentHidden(ComponentEvent e) {
            }
            public void componentShown(ComponentEvent e) {
            }
        });
    }

    protected void refreshSlices() {
        initSeperatorLinesAngles();
        repaint(getBounds());
    }

    protected void initSeperatorLinesAngles(){
        this.seperatorLinesAngles = new double[slices.size()];
        double startAngle = 0;
        for (int i = 0; i < slices.size(); i++) {
            startAngle += slices.get(i).getAngleSize();
            this.seperatorLinesAngles[i] = startAngle % 360;
        }
    }

    public static class Slice {
        protected double value;
        private Color color;
        protected java.awt.Color awt_color;
        transient protected PieGraph pieGraph;

        Slice(double value, Color color, PieGraph pieGraph) {
            this.value = value;
            this.color = color;
            this.awt_color = SettingsPanel.colorJFXToAWT(color);
            this.pieGraph = pieGraph;
            this.pieGraph.addSlice(this);
        }

        public void setAngleSize(double angleSize) {
            value = angleSize / 360 * pieGraph.totalValue;
        }

        public double getAngleSize() {
            return value / pieGraph.totalValue * 360;
        }

        public void remove() {
            pieGraph.removeSlice(this);
        }

        public double getWeight() {
            return value / pieGraph.totalValue;
        }

        public void setWeight(double weight) {
            value = weight * pieGraph.totalValue;
        }

        public double getValue() {
            return value;
        }

        public void setValue(double v) {
            this.value = v;
        }

        public Color getColor() {
            return color;
        }

        public void setColor(Color c) {
            this.color = c;
            this.awt_color = SettingsPanel.colorJFXToAWT(color);
        }

        public PieGraph getPieGraph() {
            return pieGraph;
        }

        public void refreshPieGraph() {
            pieGraph.setSlices(pieGraph.slices);
            pieGraph.refreshSlices();
        }
    }

    public Slice createSlice(double value, Color color) {
        Slice slice = new Slice(value,color,this);
        return slice;
    }

    protected void addSlice(Slice slice) {
        slices.add(slice);
        totalValue += slice.value;
        refreshSlices();
    }

    protected void removeSlice(Slice slice) {
        totalValue -= slice.value;
        slices.remove(slice);
        refreshSlices();
    }

    public ArrayList<PieGraph.Slice> getSlices() {
        return slices;
    }

    public void paint(Graphics g) {
        drawPie((Graphics2D) g, getBounds(), slices);
    }

    protected double getAngleInEllipse(int x, int y) {
        double x_centered = (double)(x - this.x0);
        double y_centered = (double)(this.y0 - y);
        double angle = Math.atan2(y_centered, x_centered);
        if(angle < 0) {
            return (2 * Math.PI + angle) / Math.PI * 180;
        } else {
            return angle / Math.PI * 180;
        }
    }

    protected int getClosestSeperatorLineIdx(double angle) {
        for(int i = 0; i < seperatorLinesAngles.length; i++) {
            if(Math.abs(angle - seperatorLinesAngles[i]) <= dragInset) {
                return i;
            }
        }
        return -1;
    }

    protected void drawPie(Graphics2D g, Rectangle area,
            ArrayList<PieGraph.Slice> slices) {
        this.x0 = (area.width / 2);
        this.y0 = (area.height / 2);
        double startAngle;
        double endAngle;
        double arcAngle;
        for (int i = 0; i < slices.size(); i++) {
            if(i == 0) {
                startAngle =
                    seperatorLinesAngles[seperatorLinesAngles.length-1];
            } else {
                startAngle = seperatorLinesAngles[i-1];
            }
            endAngle = seperatorLinesAngles[i];
            arcAngle = slices.get(i).getAngleSize();
            g.setColor(slices.get(i).awt_color);
            g.fillArc(area.x, area.y, area.width, area.height,
                    (int) startAngle, (int) Math.ceil(arcAngle));
            if(endAngle < startAngle) {
                g.fillArc(area.x, area.y, area.width, area.height,
                        0, (int) Math.ceil(endAngle));
            }
        }
    }

    private void setSlices(ArrayList<PieGraph.Slice> slices) {
        this.totalValue = 0.0D;
        for (int i = 0; i < slices.size(); i++) {
            this.totalValue += slices.get(i).value;
        }
        this.slices = slices;
    }

    public void setDragInset(int dragInset) {
        if(dragInset < 0) {
            String message = "drag inset must be greater than 0";
            throw new IllegalArgumentException(message);
        }

        this.dragInset = dragInset;
    }

    protected class PieGraphMouseAdapter extends MouseAdapter {
        private PieGraph component;
        private int lineIdx;

        protected int prevDraggedY;
        private int prevDirection = -1;
        private int direction = 0;

        private Cursor sourceCursor;
        protected boolean resizing = false;
        protected boolean inResizingBounds = false;
        private Point pressed;

        PieGraphMouseAdapter(PieGraph component) {
            this.component = component;
        }

        protected void setMouseCursor(double angle) {
            if(angle < 2) {
                direction = NORTH;
            } else if(angle < 89) {
                direction = EAST + SOUTH;
            } else if(angle < 92) {
                direction = EAST;
            } else if(angle < 179) {
                direction = WEST + SOUTH;
            } else if(angle < 182) {
                direction = NORTH;
            } else if(angle < 269) {
                direction = WEST + NORTH;
            } else if(angle < 272) {
                direction = EAST;
            } else if(angle < 359) {
                direction = EAST + NORTH;
            } else {
                direction = NORTH;
            }

            if(direction != prevDirection) {
                // use the appropriate resizable cursor
                int cursorType = PieGraph.cursors.get(direction);
                Cursor cursor = Cursor.getPredefinedCursor(cursorType);
                this.component.setCursor(cursor);
                prevDirection = direction;
            }
        }

        protected void resetMouseCursor() {
            this.component.setCursor(sourceCursor);
            prevDirection = 0;
            direction = 0;
            prevDraggedY = 0;
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            Point location = e.getPoint();
            //SwingUtilities.convertPointToScreen(location, this.component);
            double angle = getAngleInEllipse(location.x,location.y);
            int lineIdx = getClosestSeperatorLineIdx(angle);

            // Mouse is no longer over a resizable border
            if (lineIdx == -1) {
                this.inResizingBounds = false;
                resetMouseCursor();
            } else { // use the appropriate resizable cursor
                this.inResizingBounds = true;
                setMouseCursor(angle);
            }
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            if (!resizing) {
                sourceCursor = this.component.getCursor();
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            if (!resizing) {
                resetMouseCursor();
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            // The mouseMoved event continually updates this variable

            if (direction == 0)
                return;

            // Setup for resizing. All future dragging calculations are done
            // based on the original bounds of the component and mouse
            // pressed location.

            resizing = true;

            pressed = e.getPoint();
            prevDraggedY = pressed.y;
            //SwingUtilities.convertPointToScreen(pressed, this.component);
            double angle = getAngleInEllipse(pressed.x,pressed.y);
            lineIdx = getClosestSeperatorLineIdx(angle);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            resizing = false;

            resetMouseCursor();
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (resizing == false)
                return;

            Point dragged = e.getPoint();
            //SwingUtilities.convertPointToScreen(dragged, this.component);

            this.component.changeSlices(lineIdx, pressed, dragged);
        }
    }

    protected void changeSlices(int lineIdx, Point pressed, Point dragged) {
        double draggedAngle = getAngleInEllipse(dragged.x,dragged.y);
        double angle = seperatorLinesAngles[lineIdx];
        if(draggedAngle < 90 && angle > 270) {
            if(dragged.y < mouseAdapter.prevDraggedY) {
                draggedAngle += 360;
            }
        } else if(draggedAngle > 270 && angle < 90) {
            if(dragged.y > mouseAdapter.prevDraggedY) {
                angle += 360;
            }
        }
        double addAngleSize = draggedAngle - angle;
        mouseAdapter.prevDraggedY = dragged.y;

        mouseAdapter.setMouseCursor(draggedAngle);

        if(addAngleSize > 0) {
            PieGraph.Slice slice = slices.get(lineIdx);
            double sliceAngleSize = slice.getAngleSize();
            double sliceNewAngleSize = Math.min(sliceAngleSize+addAngleSize,
                    360-((slices.size()-1)*minimumAngleSize));
            double sliceAngleSizeChange = sliceNewAngleSize - sliceAngleSize;
            slice.setAngleSize(sliceNewAngleSize);
            seperatorLinesAngles[lineIdx] = (seperatorLinesAngles[lineIdx]
                    + sliceAngleSizeChange) % 360;
            int length = lineIdx + seperatorLinesAngles.length;
            double leftOverAngleSize = sliceAngleSizeChange;
            int idx;
            for(int i = lineIdx + 1; i < length; i++) {
                idx = i % seperatorLinesAngles.length;
                slice = slices.get(idx);
                sliceAngleSize = slice.getAngleSize();
                sliceNewAngleSize = Math.max(
                        sliceAngleSize-leftOverAngleSize, minimumAngleSize);
                sliceAngleSizeChange = sliceNewAngleSize - sliceAngleSize;
                slice.setAngleSize(sliceNewAngleSize);
                leftOverAngleSize += sliceAngleSizeChange;
                if(leftOverAngleSize == 0) {
                    break;
                } else {
                    seperatorLinesAngles[idx] = (seperatorLinesAngles[idx]
                            + leftOverAngleSize) % 360;
                }
            }
            repaint(getBounds());
        } else if (addAngleSize < 0) {
            int idx = (lineIdx+1) % slices.size();
            PieGraph.Slice slice = slices.get(idx);
            double sliceAngleSize = slice.getAngleSize();
            double sliceNewAngleSize = Math.min(sliceAngleSize-addAngleSize,
                    360-((slices.size()-1)*minimumAngleSize));
            double sliceAngleSizeChange = sliceNewAngleSize - sliceAngleSize;
            slice.setAngleSize(sliceNewAngleSize);
            if(sliceAngleSizeChange > seperatorLinesAngles[lineIdx]) {
                seperatorLinesAngles[lineIdx] += 360 - sliceAngleSizeChange;
            } else {
                seperatorLinesAngles[lineIdx] -= sliceAngleSizeChange;
            }
            int length = idx - seperatorLinesAngles.length;
            double leftOverAngleSize = sliceAngleSizeChange;
            int prevIdx;
            for(int i = lineIdx; i > length; i--) {
                if(i < 0) {
                    idx = seperatorLinesAngles.length + i;
                } else {
                    idx = i;
                }
                slice = slices.get(idx);
                sliceAngleSize = slice.getAngleSize();
                sliceNewAngleSize = Math.max(
                        sliceAngleSize-leftOverAngleSize, minimumAngleSize);
                sliceAngleSizeChange = sliceNewAngleSize - sliceAngleSize;
                slice.setAngleSize(sliceNewAngleSize);
                leftOverAngleSize += sliceAngleSizeChange;
                if(leftOverAngleSize == 0) {
                    break;
                } else {
                    if(i > 0) {
                        prevIdx = i - 1;
                    } else {
                        prevIdx = seperatorLinesAngles.length + i - 1;
                    }
                    if(leftOverAngleSize > seperatorLinesAngles[prevIdx]) {
                        seperatorLinesAngles[prevIdx] += 360
                            - leftOverAngleSize;
                    } else {
                        seperatorLinesAngles[prevIdx] -= leftOverAngleSize;
                    }
                }
            }
            repaint(getBounds());
        }
    }

    public boolean isResizing() {
        return mouseAdapter.inResizingBounds || mouseAdapter.resizing;
    }
}
