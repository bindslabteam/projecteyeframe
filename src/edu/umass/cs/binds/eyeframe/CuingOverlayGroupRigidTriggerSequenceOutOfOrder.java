package edu.umass.cs.binds.eyeframe;

import java.util.LinkedList;
import java.util.Map;

class CuingOverlayGroupRigidTriggerSequenceOutOfOrder extends CuingOverlayGroupWithTriggerSequence {
    CuingOverlayGroupRigidTriggerSequenceOutOfOrder(String name,
            CuingOverlayGroupManager manager,
            int triggerSequencePos) {
        super(name,
                RunState.RIGID_TRIGGER_SEQUENCE_OUTOFORDER,
                manager,
                triggerSequencePos);
    }

    public void focusedOnOverlay(CuingOverlay overlay,
            CuingOverlay lastOverlay, Stack stack) {
        if(stack.getCurrentSequenceGroup() == this) {
            if(overlay == currentSequence.first()) {
                currentSequence.remove();
            }
        } else if(overlay == currentSequence.first()
                && !stack.isASequenceGroup(this)) {
            if(overlays.size()-currentSequence.size() == triggerSequencePos) {
                currentSequence.remove();
                stack.addGroupSequence(this,currentSequence);
            } else {
                currentSequence.remove();
            }
        }
    }
}
