package edu.umass.cs.binds.eyeframe;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.awt.event.ActionListener;
import javax.swing.Action;
import javax.swing.JButton;

public class TableButtonRenderer extends JButton
        implements TableCellRenderer {
    ActionListener actionListener = null;

    public TableButtonRenderer(String text) {
        super(text);
        setOpaque(true); //MUST do this for background to show up.
    }

    public Component getTableCellRendererComponent(
            JTable table, Object actionListener,
            boolean isSelected, boolean hasFocus,
            int row, int column) {
        if(this.actionListener != actionListener) {
            if(actionListener != null) {
                this.removeActionListener(this.actionListener);
            }
            this.actionListener = (ActionListener) actionListener;
            this.addActionListener(this.actionListener);
        }
        if(isSelected && hasFocus) {
            this.doClick();
        }
        return this;
    }
}

