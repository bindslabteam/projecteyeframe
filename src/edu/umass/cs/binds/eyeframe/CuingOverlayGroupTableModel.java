package edu.umass.cs.binds.eyeframe;

import javax.swing.Box;
//import javax.swing.Box.Filler;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.JComboBox;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Iterator;
import java.awt.Dimension;
import java.awt.Component;

import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
//import javax.swing.JSpinner.NumberEditor;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableColumnModel;

import javafx.scene.paint.Color;

import javax.swing.DefaultCellEditor;
import javax.swing.JScrollPane;
import javax.swing.SpinnerNumberModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

class CuingOverlayGroupTableModel
        extends AbstractImprovedTableModel<CuingOverlayGroup> {
    protected Dimension colorBoxDimensions;
    protected JComboBox<CuingOverlayGroup.RunState> comboBox_runStates;

    /**
     * @wbp.parser.constructor
     */
    CuingOverlayGroupTableModel() {
        this(new ArrayList<CuingOverlayGroup>(),new Dimension(20,20));
    }

    CuingOverlayGroupTableModel(ArrayList<CuingOverlayGroup> groups) {
        this(groups,new Dimension(20,20));
    }

    CuingOverlayGroupTableModel(ArrayList<CuingOverlayGroup> groups,
            Dimension colorBoxDimensions) {
        comboBox_runStates = new JComboBox<>(
                CuingOverlayGroup.RunState.values());
        this.columnNames = new String[] {
            "group",
            "Type",
            "Trigger Sequence Size",
            "Delete ?"
        };
        this.rows = groups;
        this.colorBoxDimensions = colorBoxDimensions;
        this.table = new JTable(this);
        this.table.setBackground(new java.awt.Color(240,240,240));
        TableColumnModel columnModel = this.table.getColumnModel();
        columnModel.getColumn(1).setCellEditor(
                new DefaultCellEditor(comboBox_runStates));
        columnModel.getColumn(2).setCellEditor(
                new TableSpinnerEditor(
                    new SpinnerNumberModel(1,1,100,1)));
        columnModel.getColumn(3).setCellRenderer(
                new TableButtonRenderer("x"));
    }

    protected void addGroup(CuingOverlayGroup group) {
        addRow(group);
    }

    protected void removeGroup(CuingOverlayGroup group) {
        removeRow(group);
    }

    protected CuingOverlayGroup getGroup(int row) {
        return getRow(row);
    }

    public boolean isCellEditable(int row, int col) {
        if(row == 0) {
            return false;
        } else if (col == 0 || col == 1) {
            return true;
        } else if(col == 2) {
            CuingOverlayGroup group = rows.get(row);
            if(group instanceof CuingOverlayGroupWithTriggerSequence) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public Object getValueAt(int row, int col) {
        CuingOverlayGroup group = rows.get(row);
        if(col == 0) {
            return group.getName();
        } else if(col == 1) {
            return group.getType();
        } else if(col == 2) {
            if(group instanceof CuingOverlayGroupWithTriggerSequence) {
                return ((CuingOverlayGroupWithTriggerSequence)group)
                    .getTriggerSequencePos() + 1;
            } else {
                return -1;
            }
        } else if(col == 3) {
            if(row == 0) {
                return new ActionListener() {
                    public void actionPerformed(ActionEvent e) {}
                };
            } else {
                return new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        removeGroup(group);
                        Controller.controller.removeGroup(group);
                        fireTableRowsDeleted(row,row);
                    }
                };
            }
        }
        throw new RuntimeException("Missing value for row and column");
    }

    public void setValueAt(Object value, int row, int col) {
        CuingOverlayGroup group = rows.get(row);
        if(col == 0) {
            group.setName((String) value);
        } else if(col == 1) {
            CuingOverlayGroup.RunState type =
                (CuingOverlayGroup.RunState) value;
            if(group.getType() != type) {
                CuingOverlayGroup newGroup;
                if(group instanceof CuingOverlayGroupWithTriggerSequence) {
                    newGroup =
                        Controller.controller.createGroup(
                                group.getName(),
                                type,
                                ((CuingOverlayGroupWithTriggerSequence)group)
                                .getTriggerSequencePos());
                } else {
                    newGroup =
                        Controller.controller.createGroup(
                                group.getName(),
                                type);
                }
                Iterator<CuingOverlay> overlaysIterator =
                    group.getOverlays().iterator();
                CuingOverlay overlay;
                for(; overlaysIterator.hasNext();) {
                    overlay = overlaysIterator.next();
                    overlaysIterator.remove();
                    overlay.setGroup(newGroup);
                }
                removeGroup(group);
                Controller.controller.removeGroup(group);
                fireTableRowsDeleted(row,row);
            }
        } else if(col == 2) {
            if(group instanceof CuingOverlayGroupWithTriggerSequence) {
                //if((int)value <= group.size()) {
                    ((CuingOverlayGroupWithTriggerSequence)group)
                        .setTriggerSequencePos((int)value - 1);
                //}
            }
        }
    }
}

