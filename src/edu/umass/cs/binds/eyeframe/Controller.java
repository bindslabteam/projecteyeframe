package edu.umass.cs.binds.eyeframe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JComboBox;

import javafx.application.Platform;
import javafx.scene.paint.Color;

import javax.swing.JOptionPane;

import java.awt.GridBagLayout;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.BoxLayout;

import javax.swing.border.LineBorder;
import javax.swing.JTable;
import javax.swing.JSeparator;

import java.util.EnumSet;
import javax.swing.UIManager;
import net.miginfocom.swing.MigLayout;

import com.fasterxml.jackson.jr.ob.JSON;
import com.fasterxml.jackson.jr.ob.JSON.Feature;
import javax.swing.JComboBox;

public class Controller extends JFrame {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static Controller controller;
    public static boolean isEyeTracker;

    private CuingOverlayGroupManager cuingOverlayGroupManager;
    private CuingOverlayGroup cuingOverlayDefaultGroup;

    private JTextField x;
    private JTextField y;
    private JLabel lblWidth;
    private final JTextField width = new JTextField("200");
    private JLabel lblHeight;
    private JTextField height;
    private JButton btnAdd;
    private JButton done;
    private JButton run;
    private JFileChooser fc;
    public StopButton stopButton;

    //Begin block of new UI elements for recording
    protected JTextField nameField;

    //Begin block of new variables for recording
    private JCheckBox alertButton;
    private JCheckBox recordButton;
    private PieGraph pieGraph;
    private OverlayPieGraphLegend pieGraphLegend;
    private CuingOverlayGroupTableModel groupTableModel;
    private JPanel panel;

    //private JTextField textField;

    protected Coordinates runner;
    private JTable table;
    private JSeparator separator;
    private JSeparator separator_1;
    private JTabbedPane tabbedPane;
    private JPanel panel_1;
    private JPanel panel_2;
    private JLabel lblNewLabel;
    private JTextField textField;
    private JComboBox<CuingOverlayGroup.RunState> comboBox;
    private JButton btnAddGroup;
    private JLabel lblGroup;
    private JComboBox<CuingOverlayGroup> comboBox_1;
    private JComboBox<CuingOverlayGroup> comboBox_2;
    private JLabel lblSelectGroup;

    public static enum RunState {
        ALERT, RECORD
    }

    /**
     * Create the panel.
     *
     * @param isEyeTracker
     */
    public Controller(boolean isEyeTracker) {
        Controller.controller = this;
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                Controller.controller.removeAll();
                Controller.controller.stopButton.dispose();
                Controller.controller.dispose();
                Controller.controller.cuingOverlayGroupManager.remove();
                Controller.controller.cuingOverlayGroupManager = null;
                Controller.controller = null;
            }
        });

        stopButton = new StopButton();
        Controller.isEyeTracker = isEyeTracker;
        cuingOverlayGroupManager = new CuingOverlayGroupManager();
        cuingOverlayDefaultGroup = new CuingOverlayGroup(
                "No Group",
                cuingOverlayGroupManager);
        cuingOverlayDefaultGroup.setAsDefaultGroup();
        fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("eyeframe", "eyeframe");
        fc.setFileFilter(filter);


        tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        getContentPane().add(tabbedPane, BorderLayout.CENTER);

        /*
         * Groups
         */
        panel_1 = new JPanel();
        tabbedPane.addTab(null, null, panel_1, null);
        panel_1.setLayout(new MigLayout("", "[10.00][][grow 90][3mm][grow 90][3mm][][grow 70]", "[][][grow]"));

        lblNewLabel = new JLabel("Group Name");
        panel_1.add(lblNewLabel, "cell 1 0,alignx trailing");

        textField = new JTextField();
        panel_1.add(textField, "cell 2 0,growx");
        textField.setColumns(10);

        comboBox = new JComboBox<>(
                CuingOverlayGroup.RunState.values());
        comboBox.setFont(UIManager.getFont("ComboBox.font"));
        panel_1.add(comboBox, "cell 4 0,growx");

        btnAddGroup = new JButton("Add Group");
        panel_1.add(btnAddGroup, "cell 6 0");

        groupTableModel =
            new CuingOverlayGroupTableModel();
        groupTableModel.initScrollPane();
        panel_1.add(groupTableModel.getScrollPane(), "cell 1 2 6 1,grow");
        tabbedPane.setTabComponentAt(0, new JLabel("Groups"));
        groupTableModel.addRow(cuingOverlayDefaultGroup);

        btnAddGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(textField.getText().trim().length() == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Enter a name for the group.",
                            "EyeFrame Error",
                            JOptionPane.INFORMATION_MESSAGE);
                } else if(cuingOverlayGroupManager.groupNameIsTaken(
                            textField.getText().trim())) {
                    JOptionPane.showMessageDialog(null,
                            "The Group Name is already taken. Choose a different group name.",
                            "EyeFrame Error", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    createGroup();
                }
            }
        });

        /*
         * Overlays
         */
        JPanel panelOverlays = new JPanel();
        tabbedPane.addTab(null, null, panelOverlays, null);
        tabbedPane.setTabComponentAt(1, new JLabel("Overlays"));

        MigLayout migLayoutOverlays = new MigLayout("", "[][][growprio 50,grow][3mm:n][][growprio 50,grow][5mm:n][][3mm:n][3mm:n][:0mm:0mm][3mm:n][][][3mm:n][][grow,fill]", "[][][][5mm:n][][][grow][5mm:n]");
        panelOverlays.setLayout(migLayoutOverlays);

        JLabel lblName = new JLabel("Window Name");
        lblName.setFont(UIManager.getFont("Label.font"));
        panelOverlays.add(lblName, "cell 1 1,alignx center,aligny center");

        //EDIT: added components
        nameField = new JTextField();
        nameField.setFont(UIManager.getFont("TextField.font"));
        lblName.setLabelFor(nameField);
        panelOverlays.add(nameField, "cell 2 1 4 1,growx,aligny center");
        nameField.setColumns(10);

        separator = new JSeparator();
        panelOverlays.add(separator, "cell 0 0,alignx center,aligny center");

        lblWidth = new JLabel("Width:");
        lblWidth.setFont(UIManager.getFont("Label.font"));
        panelOverlays.add(lblWidth, "cell 7 1,alignx center,aligny center");
        width.setFont(UIManager.getFont("TextField.font"));
        panelOverlays.add(width, "cell 8 1 3 1,growx,aligny center");
        width.setColumns(5);

        lblHeight = new JLabel("Height:");
        lblHeight.setFont(UIManager.getFont("Label.font"));
        panelOverlays.add(lblHeight, "cell 12 1,alignx center,aligny center");

        height = new JTextField("200");
        height.setFont(UIManager.getFont("TextField.font"));
        panelOverlays.add(height, "cell 13 1,growx,aligny center");
        height.setColumns(5);

        separator_1 = new JSeparator();
        panelOverlays.add(separator_1, "cell 0 0,alignx center,aligny center");
        
        btnAdd = new JButton("Add Window");
        btnAdd.setFont(UIManager.getFont("Button.font"));
        panelOverlays.add(btnAdd, "cell 15 1,growx,aligny top");
        
        btnAdd.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (x.getText().length() == 0 || y.getText().length() == 0
                        || width.getText().length() == 0
                        || height.getText().length() == 0) {
                    Object[] options = { "OK" };
                    JOptionPane.showOptionDialog(new JFrame(), "Please enter a numerical value for all text fields ",
                            "Invalid", JOptionPane.PLAIN_MESSAGE, JOptionPane.QUESTION_MESSAGE, null, options,
                            options[0]);

                } else if(cuingOverlayGroupManager.overlayNameIsTaken(
                            nameField.getText().trim())) {
                    JOptionPane.showMessageDialog(null,
                            "The Window Name is already taken. Choose a different window name.",
                            "EyeFrame Error", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    createOverlay();
                }

            }

        });
        
        lblGroup = new JLabel("Group");
        panelOverlays.add(lblGroup, "cell 1 2,alignx trailing");
        
        comboBox_1 = new JComboBox<>(
                cuingOverlayGroupManager.getListOfGroups()
                .toArray(new CuingOverlayGroup[0]));
        panelOverlays.add(comboBox_1, "cell 2 2 4 1,growx");
        comboBox_1.setSelectedItem(cuingOverlayGroupManager.getDefaultGroup());

        JLabel lblXCoordinate = new JLabel("X:");
        lblXCoordinate.setFont(UIManager.getFont("Label.font"));
        panelOverlays.add(lblXCoordinate, "cell 7 2,alignx center,aligny center");

        x = new JTextField("200");
        x.setFont(UIManager.getFont("TextField.font"));
        panelOverlays.add(x, "cell 8 2 3 1,growx,aligny center");
        x.setColumns(5);

        JLabel lblYCoordinate = new JLabel("Y: ");
        lblYCoordinate.setFont(UIManager.getFont("Label.font"));
        panelOverlays.add(lblYCoordinate, "cell 12 2,alignx center,aligny center");

        y = new JTextField("200");
        y.setFont(UIManager.getFont("TextField.font"));
        panelOverlays.add(y, "cell 13 2,growx,aligny center");
        y.setColumns(5);
        
        lblSelectGroup = new JLabel("Select Group");
        panelOverlays.add(lblSelectGroup, "cell 7 4 2 1,alignx trailing");
        
        comboBox_2 = new JComboBox<>(
                cuingOverlayGroupManager.getListOfGroups()
                .toArray(new CuingOverlayGroup[0]));
        panelOverlays.add(comboBox_2, "cell 10 4 6 1,growx");
        comboBox_2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               CuingOverlayGroup group =
                   (CuingOverlayGroup) comboBox_2.getSelectedItem();
               if(group != null) {
                   pieGraphLegend.setCuingOverlayGroup(group);
                   pieGraphLegend.setSlices(
                           (ArrayList<OverlayPieGraphLegend.Slice>)
                           group.getOverlays().stream()
                           .map(CuingOverlay::getPieSlice)
                           .collect(
                               Collectors.toCollection(
                                   ArrayList<OverlayPieGraphLegend.Slice>::new)));
               }
            }
        });

        panel = new JPanel();
        panelOverlays.add(panel, "cell 0 5 7 1,width 90mm:35%:null,grow");
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        pieGraph = new PieGraph();
        panel.add(pieGraph);

        // Create the label table
        Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
        labelTable.put(new Integer(1), new JLabel("Low"));
        labelTable.put(new Integer(9), new JLabel("Default"));
        labelTable.put(new Integer(17), new JLabel("High"));

        pieGraphLegend = new OverlayPieGraphLegend();
        pieGraphLegend.setCuingOverlayGroup(cuingOverlayDefaultGroup);
        table = pieGraphLegend.getTable();
        pieGraphLegend.initScrollPane();
        panelOverlays.add(pieGraphLegend.getScrollPane(), "cell 7 5 9 1,grow,wmin pref");
        //        JPanel pnle = new JPanel();
        //        panelOverlays.add(pnle, "cell 7 4 7 1,grow");

        /*
         * Run
         */
        panel_2 = new JPanel();
        tabbedPane.addTab(null, null, panel_2, null);
        tabbedPane.setTabComponentAt(2, new JLabel("Run"));
        MigLayout migLayoutRun = new MigLayout("", "[][][growprio 50,grow][3mm:n][][growprio 50,grow][5mm:n][][3mm:n][][3mm:n][][grow,fill]", "[]");
        panel_2.setLayout(migLayoutRun);

        alertButton = new JCheckBox("Alert");
        alertButton.setFont(UIManager.getFont("CheckBox.font"));
        panel_2.add(alertButton, "cell 1 0 2 1,alignx center,aligny center");

        recordButton = new JCheckBox("Record");
        recordButton.setFont(UIManager.getFont("CheckBox.font"));
        panel_2.add(recordButton, "cell 4 0 2 1,alignx center,aligny center");

        done = new JButton("Save");
        done.setFont(UIManager.getFont("Button.font"));
        panel_2.add(done, "cell 7 0,grow");

        run = new JButton("Run");
        run.setFont(UIManager.getFont("Button.font"));
        panel_2.add(run, "cell 9 0 2 1,aligny top,grow");
        run.addActionListener(new ActionListener() {
            // changes colors based on ordering of the stack
            @Override
            public void actionPerformed(ActionEvent e) {
                start();
            }
        });

        done.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                File userHome = new File(System.getProperty("user.home"));
                if (userHome.exists()) {
                    fc.setCurrentDirectory(userHome);
                }
                int returnVal = fc.showSaveDialog(Controller.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    if(file.isFile()) {
                        int dialogResult = JOptionPane.showConfirmDialog(null,
                                "The file exists; Overwrite the current file?",
                                "Existing File", JOptionPane.YES_NO_OPTION);
                        if (dialogResult == JOptionPane.YES_OPTION) {
                            save(file.getAbsolutePath());
                        }
                    } else {
                        save(file.getAbsolutePath());
                    }
                }
            }
        });

        setPreferredSize(migLayoutOverlays.preferredLayoutSize(this));
        //setBounds(40, 5, 768, 703);
        setLocationByPlatform(true);
        setSize(migLayoutOverlays.preferredLayoutSize(this));
    }

    public void start(EnumSet<RunState> runState, boolean useEyeTracker) {
        setVisible(false);
        //EDIT: made windows invisible if recording mode is selected
        if(!runState.contains(RunState.ALERT)){
            cuingOverlayGroupManager.disableAlerting();
        }

        IRecorder recorder;
        if(runState.contains(RunState.RECORD)){
            recorder = new Recorder();
        } else {
            recorder = new RecorderDummy();
        }
        Stack stack = new Stack(getListOfOverlays(), recorder);
        // see if EyeTracker or Mouse is selected
        if (useEyeTracker) {
            runner = new EyeTrackerSetup(stack,cuingOverlayGroupManager);
        } else {
            runner = new MouseSetup(stack,cuingOverlayGroupManager);
        }
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                runner.start();

                stopButton.eyeFrameRunStarted();
            }
        });
    }

    private void start() {
        if(!recordButton.isSelected() && !alertButton.isSelected()) {
            JOptionPane.showMessageDialog(null,
                    "You cannot have both Recording and Alert deselected",
                    "EyeFrame Error", JOptionPane.INFORMATION_MESSAGE);
        } else {
            EnumSet<RunState> runState;
            if(recordButton.isSelected()) {
                if(alertButton.isSelected()) {
                    runState = EnumSet.of(RunState.ALERT,RunState.RECORD);
                } else {
                    runState = EnumSet.of(RunState.RECORD);
                }
            } else {
                runState = EnumSet.of(RunState.ALERT);
            }
            start(runState,isEyeTracker);
        }
    }

    public void stop() {
        stopButton.eyeFrameRunStopped();
        runner.stop();
        setVisible(true);
    }

    private void createGroup() {
        String name = textField.getText().trim();
        textField.setText("");
        createGroup(
                name,
                (CuingOverlayGroup.RunState) comboBox.getSelectedItem());
    }

    public CuingOverlayGroup createGroup(String name,
            CuingOverlayGroup.RunState type) {
        return createGroup(name,type,0);
    }

    public CuingOverlayGroup createGroup(String name,
            CuingOverlayGroup.RunState type,
            int triggerSequencePos) {
        CuingOverlayGroup group = CuingOverlayGroup
            .createCuingOverlayGroup(name,type,cuingOverlayGroupManager,
                    triggerSequencePos);
        addGroupToUI(group);
        return group;
    }

    public void addGroupToUI(CuingOverlayGroup group) {
        comboBox_1.addItem(group);
        comboBox_2.addItem(group);
        groupTableModel.addRow(group);
    }

    public void removeGroup(CuingOverlayGroup group) {
        group.remove();
        comboBox_1.removeItem(group);
        comboBox_2.removeItem(group);
    }

    private void createOverlay() {
        int xValue = Integer.parseInt(x.getText());
        int yValue = Integer.parseInt(y.getText());
        int heightValue = Integer.parseInt(height.getText());
        int widthValue = Integer.parseInt(width.getText());
        String nameValue = nameField.getText().trim();
        nameField.setText("");
        if(nameValue.isEmpty()) {
            createOverlay(xValue,yValue,
                    heightValue,widthValue,
                    getUniqueName(),
                    (CuingOverlayGroup) comboBox_1.getSelectedItem());
        } else {
            createOverlay(xValue,yValue,
                    heightValue,widthValue,
                    nameValue,
                    (CuingOverlayGroup) comboBox_1.getSelectedItem());
        }
    }

    public CuingOverlay createOverlay(int xValue, int yValue,
            int heightValue, int widthValue,
            String nameValue,
            CuingOverlayGroup group) {
        CuingOverlay addWindow = new CuingOverlay(xValue, yValue,
                heightValue, widthValue, nameValue, group,
                pieGraph, pieGraphLegend);
        return addWindow;
    }

    public CuingOverlay createOverlay(int xValue, int yValue,
            int heightValue, int widthValue,
            String nameValue) {
        CuingOverlay addWindow = new CuingOverlay(xValue, yValue,
                heightValue, widthValue, nameValue,
                cuingOverlayDefaultGroup,
                pieGraph, pieGraphLegend);
        return addWindow;
    }

    private void loadOverlayGroupManagerConfigFile(File file) {
        try {
            FileInputStream fileIn = new FileInputStream(
                    file.getAbsolutePath());
            Map<String,Object> cuingOverlayGroupManagerMap =
                JSON.std.mapFrom(fileIn);
            cuingOverlayGroupManager =
                CuingOverlayGroupManager.toObject(
                        cuingOverlayGroupManagerMap,
                        pieGraph,
                        pieGraphLegend);
            cuingOverlayDefaultGroup = cuingOverlayGroupManager.getDefaultGroup();
            fileIn.close();
        } catch (FileNotFoundException c) {
            c.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (NullPointerException r) {
            r.printStackTrace();
        } catch (Exception r) {
            r.printStackTrace();
        }
    }

    public void load(String filePath) {
        load(new File(filePath));
    }

    public void load(File file) {
        // remove windows on the screen
        groupTableModel.removeRow(cuingOverlayDefaultGroup);
        removeGroup(cuingOverlayDefaultGroup);
        cuingOverlayDefaultGroup = null;
        cuingOverlayGroupManager.remove();
        cuingOverlayGroupManager = null;
        loadOverlayGroupManagerConfigFile(file);
    }

    public void save(String filePath) {
        if (!filePath.toLowerCase().endsWith(".eyeframe")) {
            filePath = filePath + ".eyeframe";
        }
        Map<String,Object> cuingOverlayGroupManagerMap =
            cuingOverlayGroupManager.toMap();
        try {
            JSON.std
                .with(Feature.PRETTY_PRINT_OUTPUT)
                .write(cuingOverlayGroupManagerMap, new File(filePath));
        } catch(FileNotFoundException ex) {
            ex.printStackTrace();
            //TODO: print error
        } catch(IOException ex) {
            ex.printStackTrace();
            //TODO: print error
        }
    }

    public static HashSet<CuingOverlay> getListOfOverlays() {
        return Controller.controller.cuingOverlayGroupManager.getListOfOverlays();
    }

    public static CuingOverlayGroupManager getCuingOverlayGroupManager() {
        return Controller.controller.cuingOverlayGroupManager;
    }

    public static String getUniqueName() {
        return Long.toString(System.currentTimeMillis());
    }
}
