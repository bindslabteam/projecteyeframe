package edu.umass.cs.binds.eyeframe;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;


import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef.HWND;

import javafx.application.Platform;
import javafx.scene.paint.Color;

import java.awt.event.HierarchyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.HierarchyEvent;
import java.awt.event.WindowEvent;
import com.sun.jna.Native;
import java.util.HashMap;
import java.util.Map;
import javax.swing.border.Border;

public class CuingOverlay extends ClickThrough {
    //TODO: remove this field
    public static int count = 1;
    public abstract static class Group {
        abstract protected void addOverlay(CuingOverlay overlay);
        abstract protected void addOverlay(CuingOverlay overlay, int index);
        abstract protected void removeOverlay(CuingOverlay overlay);
        abstract protected void moveOverlay(int fromIndex, int toIndex);
        abstract protected int getOverlayPosition(CuingOverlay overlay);
    }

    transient WindowControlsController cr;

    // weightValue default is 1
    protected OverlayPieGraphLegend.Slice weightSlice;

    //EDIT: added name field
    private String name;

    private long minimumFocusDelay;

    private CuingOverlayGroup group;

    private boolean alertingEnabled = true;
    private boolean isInWindow = false;

    /**
     * @wbp.parser.constructor
     */
    CuingOverlay(double x, double y, double height, double width, String n,
            CuingOverlayGroup group,
            PieGraph pieGraph,
            OverlayPieGraphLegend pieGraphLegend) {
        this(x,y,height,width,n);
        this.setGroup(group);
        this.weightSlice = pieGraphLegend.createSlice(1,pieGraph,this);
    }

    private CuingOverlay(double x, double y, double height, double width,
            String n, CuingOverlayGroup group,
            double weightValue, Color sliceColor, PieGraph pieGraph,
            OverlayPieGraphLegend pieGraphLegend) {
        this(x,y,height,width,n);
        this.setGroup(group);
        this.weightSlice = pieGraphLegend.createSlice(weightValue,
                sliceColor,pieGraph,this);
    }

    private CuingOverlay(double x, double y, double height, double width,
            String n) {
        super(x,y,height,width);
        this.name = n;
        this.minimumFocusDelay = -1;
        this.alertingEnabled = true;
        this.isInWindow = false;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                cr = WindowControlsController.loadAndSetup(stage,scene,rectangle);
                cr.enable();
                setToSliceColor();
                setVisible(true);
            }
        });
    }

    protected void eyeFrameRunStarted() {
        cr.disable();
        if(SettingsPanel.isBorder) {
            setBackground(Color.TRANSPARENT);
        }
    }

    protected void eyeFrameRunStopped() {
        cr.enable();
        alertingEnabled = true;
        if(SettingsPanel.isBorder) {
            setBackground(almostTransparentColor);
        }
        setToSliceColor();
    }

    public double getWeightValue() {
        return weightSlice.getWeight();
    }

    public double getHeight() {
        return rectangle.getHeight();
    }

    public double getWidth() {
        return rectangle.getWidth();
    }

    public double getPosX() {
        return stage.getX();
    }

    public double getPosY() {
        return stage.getY();
    }

    public String getName() {
        return name;
    }

    public void setName(String n) {
        name = n;
    }

    public boolean nameIsTakenByOthers(String n) {
        return group.getManager().overlayNameIsTakenByOthers(this,n);
    }

    public void setMinimumFocusDelay(long minimumFocusDelay) {
        this.minimumFocusDelay = minimumFocusDelay;
    }

    public long getMinimumFocusDelay() {
        if(minimumFocusDelay == -1) {
            return SettingsPanel.defaultMinimumFocusDelay;
        } else {
            return minimumFocusDelay;
        }
    }

    public void setGroup(CuingOverlayGroup group) {
        if(this.group != group) {
            if(this.group != null) {
                this.group.removeOverlay(this);
            }
            this.group = group;
            group.addOverlay(this);
        }
    }

    public void setGroup(CuingOverlayGroup group, int index) {
        if(this.group != group) {
            if(this.group != null) {
                this.group.removeOverlay(this);
            }
            this.group = group;
            group.addOverlay(this,index);
        }
    }

    public CuingOverlayGroup getGroup() {
        return group;
    }

    public void setGroupPosition(int index) {
        this.group.moveOverlay(getGroupPosition(),index);
    }

    public int getGroupPosition() {
        return group.getOverlayPosition(this);
    }

    public OverlayPieGraphLegend.Slice getPieSlice() {
        return weightSlice;
    }

    public void remove() {
        group.removeOverlay(this);
        remove_();
    }

    protected void remove_() {
        weightSlice.remove();
        dispose();
    }

    public boolean containsLocation(double x, double y) {
        double posX = getPosX();
        double posY = getPosY();
        double height = getHeight();
        double width = getWidth();
        return (x >= posX
                && x <= (posX + width)
                && y >= posY
                && y <= (posY + height));
    }

    public void focusedOnOverlay(CuingOverlay lastOverlay, Stack stack) {
        group.focusedOnOverlay(this,lastOverlay,stack);
    }

    public void enteredWindow() {
        if(alertingEnabled) {
            isInWindow = true;
            hide();
        }
    }

    public void exitedWindow() {
        if(alertingEnabled && isInWindow) {
            show();
            isInWindow = false;
        }
    }

    public void setAlertingForNextEyeFrameRun(boolean b) {
        alertingEnabled = b;
    }

    public void setToSliceColor() {
        setColor(weightSlice.getColor());
    }

    public Map<String,Object> toMap() {
        Color color = weightSlice.getColor();
        HashMap<String,Double> sliceColor = new HashMap<>(2);
        sliceColor.put("red", color.getRed());
        sliceColor.put("green", color.getGreen());
        sliceColor.put("blue", color.getBlue());
        sliceColor.put("opacity", color.getOpacity());
        HashMap<String,Object> variables = new HashMap<>(9);
        variables.put("posX",getPosX());
        variables.put("posY",getPosY());
        variables.put("height",getHeight());
        variables.put("width",getWidth());
        variables.put("name",getName());
        variables.put("minimumFocusDelay",String.valueOf(minimumFocusDelay));
        variables.put("sliceColor",sliceColor);
        variables.put("weightValue",getWeightValue());
        return variables;
    }

    public static CuingOverlay toObject(
            Map<String,Object> map,
            CuingOverlayGroup group,
            PieGraph pieGraph,
            OverlayPieGraphLegend pieGraphLegend) {
        Map<String,Double> color = (Map<String,Double>) map.get("sliceColor");
        Color sliceColor = new Color(
                color.get("red"),
                color.get("green"),
                color.get("blue"),
                color.get("opacity"));
        CuingOverlay cuingOverlay = new CuingOverlay(
                (double) map.get("posX"),
                (double) map.get("posY"),
                (double) map.get("height"),
                (double) map.get("width"),
                (String) map.get("name"),
                group,
                (double) map.get("weightValue"),
                sliceColor,
                pieGraph,
                pieGraphLegend);
        cuingOverlay.setMinimumFocusDelay(
                Long.parseLong((String)map.get("minimumFocusDelay")));
        cuingOverlay.setColor(sliceColor);
        return cuingOverlay;
    }
}

