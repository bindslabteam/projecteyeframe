package edu.umass.cs.binds.eyeframe;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import java.awt.geom.Point2D;
import java.io.IOException;

import javafx.scene.input.MouseEvent;

/**
 * Controller implements window controls: maximise, minimise, move, and Windows Aero Snap.
 */
public class WindowControlsController {
    private Stage primaryStage;
    private Scene primaryScene;
    private Node movementControlNode;
    protected Point2D.Double prevSize;
    protected Point2D.Double prevPos;
    protected boolean maximised;
    private boolean snapped;
    private EventHandler<MouseEvent> onMousePressed;
    private EventHandler<MouseEvent> onMouseClicked;
    private EventHandler<MouseEvent> onMouseDragged;
    private EventHandler<MouseEvent> onMouseReleased;
    private Point2D.Double delta;
    private Point2D.Double eventSource;
    @FXML
    private AnchorPane rootPane;
    @FXML
    private Pane leftPane;
    @FXML
    private Pane rightPane;
    @FXML
    private Pane topPane;
    @FXML
    private Pane bottomPane;
    @FXML
    private Pane topLeftPane;
    @FXML
    private Pane topRightPane;
    @FXML
    private Pane bottomLeftPane;
    @FXML
    private Pane bottomRightPane;

    public static WindowControlsController load() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(WindowControlsController.class.getResource(
                        "/edu/umass/cs/binds/eyeframe/WindowControls.fxml"));
            loader.load();
            WindowControlsController controller =
                ((WindowControlsController) loader.getController());
            return controller;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static WindowControlsController loadAndSetup(
            Stage stage,
            Scene scene,
            Node node) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(WindowControlsController.class.getResource(
                        "/edu/umass/cs/binds/eyeframe/WindowControls.fxml"));
            loader.load();
            WindowControlsController controller =
                ((WindowControlsController) loader.getController());
            controller.setStage(stage);
            controller.setScene(scene);
            controller.setMovementControlNode(node);
            return controller;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * The constructor.
     */
    public WindowControlsController() {
        prevSize = new Point2D.Double();
        prevPos = new Point2D.Double();
        maximised = false;
        snapped = false;
    }

    /**
     * Called after the FXML layout is loaded.
     */
    @FXML
    private void initialize() {
        setResizeControl(leftPane, "left");
        setResizeControl(rightPane, "right");
        setResizeControl(topPane, "top");
        setResizeControl(bottomPane, "bottom");
        setResizeControl(topLeftPane, "top-left");
        setResizeControl(topRightPane, "top-right");
        setResizeControl(bottomLeftPane, "bottom-left");
        setResizeControl(bottomRightPane, "bottom-right");
    }

    public void setStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void setScene(Scene primaryScene) {
        this.primaryScene = primaryScene;
    }

    public void enable() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Parent root = primaryScene.getRoot();
                primaryScene.setRoot(rootPane);
                ObservableList<Node> children = rootPane.getChildren();
                children.remove(0);
                children.add(0, root);
                AnchorPane.setLeftAnchor(root, Double.valueOf(0.0D));
                AnchorPane.setTopAnchor(root, Double.valueOf(0.0D));
                AnchorPane.setRightAnchor(root, Double.valueOf(0.0D));
                AnchorPane.setBottomAnchor(root, Double.valueOf(0.0D));

                delta = new Point2D.Double();
                eventSource = new Point2D.Double();
                // Record drag deltas on press.
                movementControlNode.addEventHandler(
                        MouseEvent.MOUSE_PRESSED,
                        onMousePressed);
                // Dragging moves the application around.
                movementControlNode.addEventHandler(
                        MouseEvent.MOUSE_DRAGGED,
                        onMouseDragged);
                // Maximise on double click.
                movementControlNode.addEventHandler(
                        MouseEvent.MOUSE_CLICKED,
                        onMouseClicked);
                // Aero Snap on release.
                movementControlNode.addEventHandler(
                        MouseEvent.MOUSE_RELEASED,
                        onMouseReleased);
            }
        });
    }

    public void disable() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                ObservableList<Node> children = rootPane.getChildren();
                Parent root = (Parent) children.get(0);
                children.remove(0);
                primaryScene.setRoot(root);

                // Record drag deltas on press.
                movementControlNode.removeEventHandler(
                        MouseEvent.MOUSE_PRESSED,
                        onMousePressed);
                // Dragging moves the application around.
                movementControlNode.removeEventHandler(
                        MouseEvent.MOUSE_DRAGGED,
                        onMouseDragged);
                // Maximise on double click.
                movementControlNode.removeEventHandler(
                        MouseEvent.MOUSE_CLICKED,
                        onMouseClicked);
                // Aero Snap on release.
                movementControlNode.removeEventHandler(
                        MouseEvent.MOUSE_RELEASED,
                        onMouseReleased);
            }
        });
    }

    /**
     * Maximise on/off the application.
     */
    public void maximise() {
        Rectangle2D screen;
        if (Screen.getScreensForRectangle(primaryStage.getX(), primaryStage.getY(),primaryStage.getWidth() / 2,
                    primaryStage.getHeight() / 2).size() == 0) {
            screen = ((Screen) Screen.getScreensForRectangle(primaryStage.getX(), primaryStage.getY(),
                        primaryStage.getWidth(), primaryStage.getHeight()).get(0)).getVisualBounds();
        } else {
            screen = ((Screen) Screen.getScreensForRectangle(primaryStage.getX(), primaryStage.getY(),
                        primaryStage.getWidth() / 2, primaryStage.getHeight() / 2).get(0)).getVisualBounds();
        }

        if (maximised) {
            primaryStage.setWidth(prevSize.x);
            primaryStage.setHeight(prevSize.y);
            primaryStage.setX(prevPos.x);
            primaryStage.setY(prevPos.y);
            isMaximised(false);
        } else {
            // Record position and size, and maximise.
            if (!snapped) {
                prevSize.x = primaryStage.getWidth();
                prevSize.y = primaryStage.getHeight();
                prevPos.x = primaryStage.getX();
                prevPos.y = primaryStage.getY();
            } else if (!screen.contains(prevPos.x, prevPos.y)) {
                if (prevSize.x > screen.getWidth())
                    prevSize.x = screen.getWidth() - 20;

                if (prevSize.y > screen.getHeight())
                    prevSize.y = screen.getHeight() - 20;

                prevPos.x = screen.getMinX() + (screen.getWidth() - prevSize.x)/2;
                prevPos.y = screen.getMinY() +  (screen.getHeight() - prevSize.y)/2;
            }

            primaryStage.setX(screen.getMinX());
            primaryStage.setY(screen.getMinY());
            primaryStage.setWidth(screen.getWidth());
            primaryStage.setHeight(screen.getHeight());

            isMaximised(true);
        }
    }

    /**
     * Minimise the application.
     */
    public void minimise() {
        primaryStage.setIconified(true);
    }

    /**
     * Set a node that can be pressed and dragged to move the application around.
     * @param node the node.
     */
    public void setMovementControlNode(Node node) {
        this.movementControlNode = node;

        this.onMousePressed = new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.isPrimaryButtonDown()) {
                    delta.x = mouseEvent.getX();
                    delta.y = mouseEvent.getY();

                    if (maximised || snapped) {
                        delta.x = (prevSize.x * (mouseEvent.getX() / primaryStage.getWidth()));
                        delta.y = (prevSize.y * (mouseEvent.getY() / primaryStage.getHeight()));
                    } else {
                        prevSize.x = primaryStage.getWidth();
                        prevSize.y = primaryStage.getHeight();
                        prevPos.x = primaryStage.getX();
                        prevPos.y = primaryStage.getY();
                    }

                    eventSource.x = mouseEvent.getScreenX();
                    eventSource.y = movementControlNode.prefHeight(primaryStage.getHeight());
                }
            }
        };

        this.onMouseDragged = new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.isPrimaryButtonDown()) {
                    // Move x axis.
                    primaryStage.setX(mouseEvent.getScreenX() - delta.x);

                    if (snapped) {
                        // Aero Snap off.
                        Rectangle2D screen = ((Screen) Screen.getScreensForRectangle(mouseEvent.getScreenX(),
                                    mouseEvent.getScreenY(), 1, 1).get(0)).getVisualBounds();

                        primaryStage.setHeight(screen.getHeight());

                        if (mouseEvent.getScreenY() > eventSource.y) {
                            primaryStage.setWidth(prevSize.x);
                            primaryStage.setHeight(prevSize.y);
                            snapped = false;
                        }
                    } else {
                        // Move y axis.
                        primaryStage.setY(mouseEvent.getScreenY() - delta.y);
                    }

                    // Aero Snap off.
                    if (maximised) {
                        primaryStage.setWidth(prevSize.x);
                        primaryStage.setHeight(prevSize.y);
                        isMaximised(false);
                    }
                }
            }
        };

        this.onMouseClicked = new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                if ((mouseEvent.getButton().equals(MouseButton.PRIMARY)) && (mouseEvent.getClickCount() == 2)) {
                    maximise();
                }
            }
        };

        this.onMouseReleased = new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                if ((mouseEvent.getButton().equals(MouseButton.PRIMARY)) && (mouseEvent.getScreenX() != eventSource.x)) {
                    Rectangle2D screen = ((Screen) Screen.getScreensForRectangle(mouseEvent.getScreenX(),
                                mouseEvent.getScreenY(), 1, 1).get(0)).getVisualBounds();

                    // Aero Snap Left.
                    if (mouseEvent.getScreenX() == screen.getMinX()) {
                        primaryStage.setY(screen.getMinY());
                        primaryStage.setHeight(screen.getHeight());

                        primaryStage.setX(screen.getMinX());
                        if (screen.getWidth() / 2 < primaryStage.getMinWidth()) {
                            primaryStage.setWidth(primaryStage.getMinWidth());
                        } else {
                            primaryStage.setWidth(screen.getWidth() / 2);
                        }

                        snapped = true;
                    }

                    // Aero Snap Right.
                    else if (mouseEvent.getScreenX() == screen.getMaxX() - 1) {
                        primaryStage.setY(screen.getMinY());
                        primaryStage.setHeight(screen.getHeight());

                        if (screen.getWidth() / 2 < primaryStage.getMinWidth()) {
                            primaryStage.setWidth(primaryStage.getMinWidth());
                        } else {
                            primaryStage.setWidth(screen.getWidth() / 2);
                        }
                        primaryStage.setX(screen.getMaxX() - primaryStage.getWidth());

                        snapped = true;
                    }

                    // Aero Snap Top.
                    else if (mouseEvent.getScreenY() == screen.getMinY()) {
                        if (!screen.contains(prevPos.x, prevPos.y)) {
                            if (prevSize.x > screen.getWidth())
                                prevSize.x = screen.getWidth() - 20;

                            if (prevSize.y > screen.getHeight())
                                prevSize.y = screen.getHeight() - 20;

                            prevPos.x = screen.getMinX() + (screen.getWidth() - prevSize.x)/2;
                            prevPos.y = screen.getMinY() +  (screen.getHeight() - prevSize.y)/2;
                        }

                        primaryStage.setX(screen.getMinX());
                        primaryStage.setY(screen.getMinY());
                        primaryStage.setWidth(screen.getWidth());
                        primaryStage.setHeight(screen.getHeight());
                        isMaximised(true);
                    }
                }
            }
        };
    }

    /**
     * Set pane to resize application when pressed and dragged.
     * @param pane the pane the action is set to.
     * @param direction the resize direction. Diagonal: 'top' or 'bottom' + 'right' or 'left'.
     */
    private void setResizeControl(Pane pane, final String direction) {
        pane.setOnMouseDragged(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.isPrimaryButtonDown()) {
                    double width = primaryStage.getWidth();
                    double height = primaryStage.getHeight();

                    // Horizontal resize.
                    if (direction.endsWith("left")) {
                        if ((width > primaryStage.getMinWidth()) || (mouseEvent.getX() < 0)) {
                            primaryStage.setWidth(width - mouseEvent.getScreenX() + primaryStage.getX());
                            primaryStage.setX(mouseEvent.getScreenX());
                        }
                    } else if ((direction.endsWith("right"))
                            && ((width > primaryStage.getMinWidth()) || (mouseEvent.getX() > 0))) {
                        primaryStage.setWidth(width + mouseEvent.getX());
                            }

                    // Vertical resize.
                    if (direction.startsWith("top")) {
                        if (snapped) {
                            primaryStage.setHeight(prevSize.y);
                            snapped = false;
                        } else if ((height > primaryStage.getMinHeight()) || (mouseEvent.getY() < 0)) {
                            primaryStage.setHeight(height - mouseEvent.getScreenY() + primaryStage.getY());
                            primaryStage.setY(mouseEvent.getScreenY());
                        }
                    } else if (direction.startsWith("bottom")) {
                        if (snapped) {
                            primaryStage.setY(prevPos.y);
                            snapped = false;
                        } else if ((height > primaryStage.getMinHeight()) || (mouseEvent.getY() > 0)) {
                            primaryStage.setHeight(height + mouseEvent.getY());
                        }
                    }
                }
            }
        });

        // Record application height and y position.
        pane.setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                if ((mouseEvent.isPrimaryButtonDown()) && (!snapped)) {
                    prevSize.y = primaryStage.getHeight();
                    prevPos.y = primaryStage.getY();
                }
            }
        });

        // Aero Snap Resize.
        pane.setOnMouseReleased(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                if ((mouseEvent.getButton().equals(MouseButton.PRIMARY)) && (!snapped)) {
                    Rectangle2D screen = ((Screen) Screen.getScreensForRectangle(mouseEvent.getScreenX(),
                                mouseEvent.getScreenY(), 1, 1).get(0)).getVisualBounds();

                    if ((primaryStage.getY() <= screen.getMinY()) && (direction.startsWith("top"))) {
                        primaryStage.setHeight(screen.getHeight());
                        primaryStage.setY(screen.getMinY());
                        snapped = true;
                    }

                    if ((mouseEvent.getScreenY() >= screen.getMaxY()) && (direction.startsWith("bottom"))) {
                        primaryStage.setHeight(screen.getHeight());
                        primaryStage.setY(screen.getMinY());
                        snapped = true;
                    }
                }
            }
        });

        // Aero Snap resize on double click.
        pane.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                if ((mouseEvent.getButton().equals(MouseButton.PRIMARY)) && (mouseEvent.getClickCount() == 2)
                        && ((direction.equals("top")) || (direction.equals("bottom")))) {
                    Rectangle2D screen = ((Screen) Screen.getScreensForRectangle(primaryStage.getX(), primaryStage.getY(),
                                primaryStage.getWidth() / 2, primaryStage.getHeight() / 2).get(0)).getVisualBounds();

                    if (snapped) {
                        primaryStage.setHeight(prevSize.y);
                        primaryStage.setY(prevPos.y);
                        snapped = false;
                    } else {
                        prevSize.y = primaryStage.getHeight();
                        prevPos.y = primaryStage.getY();
                        primaryStage.setHeight(screen.getHeight());
                        primaryStage.setY(screen.getMinY());
                        snapped = true;
                    }
                        }
            }
        });
    }

    public void isMaximised(Boolean maximised) {
        this.maximised = maximised;
        setResizable(!maximised);
    }

    public void setResizable(Boolean bool){
        leftPane.setDisable(!bool);
        rightPane.setDisable(!bool);
        topPane.setDisable(!bool);
        bottomPane.setDisable(!bool);
        topLeftPane.setDisable(!bool);
        topRightPane.setDisable(!bool);
        bottomLeftPane.setDisable(!bool);
        bottomRightPane.setDisable(!bool);
    }
}
