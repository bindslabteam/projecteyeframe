package edu.umass.cs.binds.eyeframe;

import java.util.LinkedList;
import java.util.Map;

class CuingOverlayGroupRigidTriggerSequenceStrictOutOfOrder extends CuingOverlayGroupWithTriggerSequence {
    private CuingOverlay lastOverlay;

    CuingOverlayGroupRigidTriggerSequenceStrictOutOfOrder(String name,
            CuingOverlayGroupManager manager,
            int triggerSequencePos) {
        super(name,
                RunState.RIGID_TRIGGER_SEQUENCE_STRICT_OUTOFORDER,
                manager,
                triggerSequencePos);
        lastOverlay = null;
    }

    public void setup() {
        super.setup();
        lastOverlay = null;
    }

    public void focusedOnOverlay(CuingOverlay overlay,
            CuingOverlay previousOverlay, Stack stack) {
        if(stack.getCurrentSequenceGroup() == this) {
            if(overlay == currentSequence.first()) {
                currentSequence.remove();
            }
        } else if(overlay == currentSequence.first()
                && !stack.isASequenceGroup(this)) {
            if(this.lastOverlay == previousOverlay) {
                if(overlays.size()-currentSequence.size() == triggerSequencePos) {
                    currentSequence.remove();
                    stack.addGroupSequence(this,currentSequence);
                } else {
                    currentSequence.remove();
                }
            } else {
                setup();
            }
        }
    }

    public Map<String,Object> toMap() {
        Map<String,Object> variables = super.toMap();
        variables.put("triggerSequencePos",triggerSequencePos);
        return variables;
    }
}

