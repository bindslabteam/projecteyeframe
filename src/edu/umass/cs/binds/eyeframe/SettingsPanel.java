package edu.umass.cs.binds.eyeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JCheckBox;
import javax.swing.JTextField;

import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.UIManager;
import java.awt.SystemColor;
import javax.swing.border.Border;

import javax.swing.JColorChooser;
import javax.swing.JDialog;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.DocumentEvent;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JSeparator;

public class SettingsPanel extends JFrame {

    private JPanel contentPane;
    private JCheckBox showBorder;
    private CuingOverlayDemo ctOne;
    private CuingOverlayDemo ctTwo;
    private CuingOverlayDemo ctRegular;
    private JTextField txtColor;
    private JLabel txtMostNeglectedWindow;
    private JLabel txtndNeglectedWindow;
    private JLabel txtRegular;
    private JLabel txtBorderThickness;
    private JLabel txtMinimumRequiredTime;
    private JLabel txtMilliseconds;
    private JButton mnColorButton;
    private JButton smnColorButton;
    private JButton regColorButton;
    private JColorChooser mnColorChooser;
    private JDialog mnColorDialog;
    private JSpinner borderThicknesSpinner;
    private CuingOverlayDemo ctColorPickedFor;
    private boolean apiServerSettingChanged;

    public static SettingsPanel settingsPanel;
    public static boolean isBorder = false;
    public static javafx.scene.paint.Color mostNeglected = new javafx.scene.paint.Color(1.0, 0, 0, 153/255.0);
    public static javafx.scene.paint.Color secondNeglected = new javafx.scene.paint.Color(1.0, 0, 0, 90/255.0);
    public static javafx.scene.paint.Color regColor = new javafx.scene.paint.Color(253/255.0, 250/255.0, 250/255.0, 127/255.0);
    public static javafx.scene.paint.Color transparent = new javafx.scene.paint.Color(0,0,0,0);
    public static int borderThickness = 5;
    public static long defaultMinimumFocusDelay;
    public static String ipAddress = "127.0.0.1";
    public static Integer port = 4223;

    private JTextField txtApiServer;
    private JLabel txtIp;
    private JLabel txtPort;
    private JSpinner spinner_port;
    private JTextField ipTextField;
    private JLabel lblOverlays;
    private JSeparator separator;
    private JSeparator separator_1;
    private JLabel lblShowOnlyBorder;

    public static javafx.scene.paint.Color colorAWTToJFX(Color color) {
        return javafx.scene.paint.Color.rgb(
                color.getRed(),
                color.getGreen(),
                color.getBlue(),
                color.getAlpha()/255.0);
    }

    public static Color colorJFXToAWT(javafx.scene.paint.Color color) {
        //new Exception().printStackTrace(System.out);
        return new Color(
                (float) color.getRed(),
                (float) color.getGreen(),
                (float) color.getBlue(),
                (float) color.getOpacity());
    }
    /**
     * Create the frame.
     */
    public SettingsPanel() {
        settingsPanel = this;
        apiServerSettingChanged = false;
        setTitle("Settings");
        //setResizable(false);
        //setBounds(100, 100, 450, 427);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                closeSettingsPanel();
            }
        });
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        ctOne = new CuingOverlayDemo(650, 82);
        ctTwo = new CuingOverlayDemo(650, 292);
        ctRegular = new CuingOverlayDemo(650, 502);
        MigLayout migLayout = new MigLayout("", "[][][][]", "[8mm:n][8mm:n][8mm:n][8mm:n][8mm:n][8mm:n][8mm:n][][8mm:n][8mm:n][8mm:n][8mm:n][8mm:n][8mm:n]");
        contentPane.setLayout(migLayout);

        lblOverlays = new JLabel("Overlay Switching");
        lblOverlays.setFont(UIManager.getFont("Label.font"));
        contentPane.add(lblOverlays, "cell 0 0,alignx leading");

        txtMinimumRequiredTime = new JLabel();
        txtMinimumRequiredTime.setFont(UIManager.getFont("Label.font"));
        txtMinimumRequiredTime.setText("Default Focus Delay");
        txtMinimumRequiredTime.setHorizontalAlignment(SwingConstants.RIGHT);
        txtMinimumRequiredTime.setBackground(SystemColor.menu);
        txtMinimumRequiredTime.setBorder(null);
        contentPane.add(txtMinimumRequiredTime, "cell 0 1,alignx trailing,aligny center");
        SpinnerModel sleepM = new SpinnerNumberModel(
                new Long(SettingsPanel.defaultMinimumFocusDelay),
                new Long(0L), new Long(10000000L), new Long(100L));
        JSpinner sleepSpinner = new JSpinner(sleepM);
        sleepSpinner.setFont(UIManager.getFont("Spinner.font"));
        sleepSpinner.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent arg0) {
                SettingsPanel.defaultMinimumFocusDelay = (Long) sleepSpinner.getValue();
            }
        });
        contentPane.add(sleepSpinner, "cell 2 1,growx");

        txtMilliseconds = new JLabel();
        txtMilliseconds.setText("msec");
        txtMilliseconds.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        txtMilliseconds.setHorizontalAlignment(SwingConstants.CENTER);
        txtMilliseconds.setBackground(SystemColor.menu);
        contentPane.add(txtMilliseconds, "cell 3 1,alignx left,growy");

        separator = new JSeparator();
        contentPane.add(separator, "cell 0 2 4 1");

        txtColor = new JTextField();
        txtColor.setFont(UIManager.getFont("Label.font"));
        txtColor.setBorder(null);
        txtColor.setBackground(UIManager.getColor("Button.background"));
        txtColor.setHorizontalAlignment(SwingConstants.LEFT);
        txtColor.setText("Overlay Color");
        contentPane.add(txtColor, "cell 0 3 3 1,alignx leading");
        txtColor.setColumns(10);

        txtMostNeglectedWindow = new JLabel();
        txtMostNeglectedWindow.setFont(UIManager.getFont("Label.font"));
        txtMostNeglectedWindow.setBorder(null);
        txtMostNeglectedWindow.setBackground(UIManager
                .getColor("Button.background"));
        txtMostNeglectedWindow.setHorizontalAlignment(SwingConstants.TRAILING);
        txtMostNeglectedWindow.setText("Most Neglected Window");
        contentPane.add(txtMostNeglectedWindow, "cell 0 4,alignx trailing,aligny center");
        SpinnerModel thickSpinner = new SpinnerNumberModel(
                SettingsPanel.borderThickness, 1, 15, 1);

        mnColorButton = new JButton();
        mnColorButton.setFont(UIManager.getFont("Button.font"));
        mnColorButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ctColorPickedFor = ctOne;
                mnColorButton.setBackground(
                        colorJFXToAWT(SettingsPanel.mostNeglected));
                mnColorChooser.setColor(
                        colorJFXToAWT(SettingsPanel.mostNeglected));
                mnColorDialog.setVisible(true);
            }
        });
        mnColorButton.setBorderPainted(false);
        mnColorButton.setBackground(colorJFXToAWT(SettingsPanel.mostNeglected));
        contentPane.add(mnColorButton, "cell 2 4,grow");

        txtndNeglectedWindow = new JLabel();
        txtndNeglectedWindow.setFont(UIManager.getFont("Label.font"));
        txtndNeglectedWindow.setBorder(null);
        txtndNeglectedWindow.setBackground(UIManager
                .getColor("Button.background"));
        txtndNeglectedWindow.setHorizontalAlignment(SwingConstants.TRAILING);
        txtndNeglectedWindow.setText("2nd Neglected Window");
        contentPane.add(txtndNeglectedWindow, "cell 0 5,alignx trailing,aligny center");
        smnColorButton = new JButton();
        smnColorButton.setFont(UIManager.getFont("Button.font"));
        smnColorButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ctColorPickedFor = ctTwo;
                smnColorButton.setBackground(
                        colorJFXToAWT(SettingsPanel.secondNeglected));
                mnColorChooser.setColor(colorJFXToAWT(SettingsPanel.secondNeglected));
                mnColorDialog.setVisible(true);
            }
        });
        smnColorButton.setBorderPainted(false);
        smnColorButton.setBackground(colorJFXToAWT(SettingsPanel.secondNeglected));
        contentPane.add(smnColorButton, "cell 2 5,grow");

        txtRegular = new JLabel();
        txtRegular.setFont(UIManager.getFont("Label.font"));
        txtRegular.setBorder(null);
        txtRegular.setBackground(UIManager.getColor("Button.background"));
        txtRegular.setHorizontalAlignment(SwingConstants.TRAILING);
        txtRegular.setText("Regular");
        contentPane.add(txtRegular, "cell 0 6,alignx trailing,aligny center");
        regColorButton = new JButton();
        regColorButton.setFont(UIManager.getFont("Button.font"));
        regColorButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ctColorPickedFor = ctRegular;
                regColorButton.setBackground(
                        colorJFXToAWT(SettingsPanel.regColor));
                mnColorChooser.setColor(colorJFXToAWT(SettingsPanel.regColor));
                mnColorDialog.setVisible(true);
            }
        });
        regColorButton.setBorderPainted(false);
        regColorButton.setBackground(colorJFXToAWT(SettingsPanel.regColor));
        contentPane.add(regColorButton, "cell 2 6,grow");

        showBorder = new JCheckBox("");
        showBorder.setFont(UIManager.getFont("CheckBox.font"));
        showBorder.setHorizontalAlignment(SwingConstants.LEFT);
        showBorder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _enableOverlayBorderMode(showBorder.isSelected());
            }
        });

        lblShowOnlyBorder = new JLabel("Show Only Border Color");
        contentPane.add(lblShowOnlyBorder, "cell 0 8,alignx right");
        showBorder.setSelected(SettingsPanel.isBorder);
        contentPane.add(showBorder, "cell 2 8,alignx center,growy");

        txtBorderThickness = new JLabel();
        txtBorderThickness.setFont(UIManager.getFont("Label.font"));
        txtBorderThickness.setBorder(null);
        txtBorderThickness.setText("Border Thickness");
        txtBorderThickness.setHorizontalAlignment(SwingConstants.TRAILING);
        txtBorderThickness.setBackground(SystemColor.menu);
        contentPane.add(txtBorderThickness, "cell 0 9,alignx trailing,aligny center");
        borderThicknesSpinner = new JSpinner(thickSpinner);
        borderThicknesSpinner.setFont(UIManager.getFont("Spinner.font"));
        borderThicknesSpinner.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent arg0) {
                _setOverlayBorderThickness((int) borderThicknesSpinner.getValue());
            }
        });
        contentPane.add(borderThicknesSpinner, "cell 2 9,growx");
        if (!showBorder.isSelected()) {
            borderThicknesSpinner.setEnabled(false);
        }

        separator_1 = new JSeparator();
        contentPane.add(separator_1, "cell 0 10 4 1");

        txtApiServer = new JTextField();
        txtApiServer.setFont(UIManager.getFont("Label.font"));
        txtApiServer.setBorder(null);
        txtApiServer.setHorizontalAlignment(SwingConstants.LEFT);
        txtApiServer.setBackground(SystemColor.menu);
        txtApiServer.setEditable(false);
        txtApiServer.setText("API Server");
        contentPane.add(txtApiServer, "cell 0 11 3 1,alignx leading,aligny center");
        txtApiServer.setColumns(10);

        txtIp = new JLabel();
        txtIp.setFont(UIManager.getFont("Label.font"));
        txtIp.setBorder(null);
        txtIp.setBackground(SystemColor.menu);
        txtIp.setText("IP");
        contentPane.add(txtIp, "cell 0 12,alignx trailing,aligny center");

        ipTextField = new JTextField();
        ipTextField.setFont(UIManager.getFont("TextField.font"));
        contentPane.add(ipTextField, "cell 2 12,growx");
        ipTextField.setColumns(10);
        ipTextField.setText(SettingsPanel.ipAddress);
        ipTextField.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                SettingsPanel.ipAddress = ipTextField.getText();
                apiServerSettingChanged = true;
            }
            public void insertUpdate(DocumentEvent e) {
                SettingsPanel.ipAddress = ipTextField.getText();
                apiServerSettingChanged = true;
            }
            public void removeUpdate(DocumentEvent e) {
                SettingsPanel.ipAddress = ipTextField.getText();
                apiServerSettingChanged = true;
            }
        });

        txtPort = new JLabel();
        txtPort.setFont(UIManager.getFont("Label.font"));
        txtPort.setBorder(null);
        txtPort.setBackground(SystemColor.menu);
        txtPort.setText("Port");
        contentPane.add(txtPort, "cell 0 13,alignx trailing,aligny center");

        spinner_port = new JSpinner();
        spinner_port.setFont(UIManager.getFont("Spinner.font"));
        spinner_port.setEditor(new JSpinner.NumberEditor(spinner_port, "#"));
        contentPane.add(spinner_port, "cell 2 13,growx");
        spinner_port.setValue(SettingsPanel.port);
        spinner_port.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent arg0) {
                SettingsPanel.port = (Integer) spinner_port.getValue();
                apiServerSettingChanged = true;
            }
        });

        mnColorChooser = new JColorChooser();
        mnColorChooser.setPreviewPanel(new JPanel());
        mnColorDialog = JColorChooser.createDialog(this,
                "Pick a Color",
                true,  //modal
                mnColorChooser,
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        changeColor(mnColorChooser.getColor(),
                                ctColorPickedFor);
                    }
                },  //OK button handler
                null); //no CANCEL button handler

        setMinimumSize(migLayout.preferredLayoutSize(this));
        setMaximumSize(migLayout.preferredLayoutSize(this));

        changeColor(colorJFXToAWT(SettingsPanel.mostNeglected), ctOne);
        changeColor(colorJFXToAWT(SettingsPanel.secondNeglected), ctTwo);
        changeColor(colorJFXToAWT(SettingsPanel.regColor), ctRegular);
    }

    protected void _enableOverlayBorderMode(boolean b) {
        if (b) {
            SettingsPanel.isBorder = true;
            borderThicknesSpinner.setEnabled(true);
            ctOne.setBorder(borderThickness,mostNeglected);
            ctTwo.setBorder(borderThickness,secondNeglected);
            ctRegular.setBorder(borderThickness,regColor);
        } else {
            SettingsPanel.isBorder = false;
            borderThicknesSpinner.setEnabled(false);
            ctOne.setBackground(SettingsPanel.mostNeglected);
            ctTwo.setBackground(SettingsPanel.secondNeglected);
            ctRegular.setBackground(SettingsPanel.regColor);
        }
    }

    public void enableOverlayBorderMode(boolean b) {
        //showBorder.setSelected(b);
        //_enableOverlayBorderMode(b);
        if(b) {
            if(!showBorder.isSelected()) {
                showBorder.doClick();
            }
        } else {
            if(showBorder.isSelected()) {
                showBorder.doClick();
            }
        }
    }

    protected void _setOverlayBorderThickness(int thickness) {
        borderThickness = thickness;
        ctOne.setColor(mostNeglected);
        ctTwo.setColor(secondNeglected);
        ctRegular.setColor(regColor);
    }

    public void setOverlayBorderThickness(int thickness) {
        borderThicknesSpinner.setValue(thickness);
    }

    public void changeColor(Color selectedColor, CuingOverlayDemo ct) {
        if(selectedColor.getAlpha() == 255) {
            selectedColor = new Color(selectedColor.getRed(),
                    selectedColor.getGreen(),
                    selectedColor.getBlue(),
                    selectedColor.getAlpha() - 1);
        }
        javafx.scene.paint.Color jfx_selectedColor = colorAWTToJFX(selectedColor);
        if (ct == ctOne) {
            SettingsPanel.mostNeglected = jfx_selectedColor;
            mnColorButton.setBackground(
                    selectedColor);
        } else if (ct == ctTwo) {
            SettingsPanel.secondNeglected = jfx_selectedColor;
            smnColorButton.setBackground(
                    selectedColor);
        } else if (ct == ctRegular) {
            SettingsPanel.regColor = jfx_selectedColor;
            regColorButton.setBackground(
                    selectedColor);
        }
        ct.setColor(jfx_selectedColor);
    }

    protected void closeSettingsPanel() {
        if(apiServerSettingChanged) {
            MainMenu.mainMenu.restartServer();
            apiServerSettingChanged = false;
        }
        settingsPanel = null;
        ctOne.dispose();
        ctTwo.dispose();
        ctRegular.dispose();
        this.dispose();
    }

    public CuingOverlayDemo getCtOne() {
        return ctOne;
    }

    public CuingOverlayDemo getCtTwo() {
        return ctTwo;
    }

    public CuingOverlayDemo getCtRegular() {
        return ctRegular;
    }
}
