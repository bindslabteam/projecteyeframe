package edu.umass.cs.binds.eyeframe;

import java.util.LinkedList;

abstract class CuingOverlayGroupWithSequence extends CuingOverlayGroup {
    protected CuingOverlayGroupSequenceQueueView currentSequence;

    CuingOverlayGroupWithSequence(String name,
            RunState type,
            CuingOverlayGroupManager manager) {
        super(name, type, manager);
        currentSequence = new CuingOverlayGroupSequenceQueueView(overlays);
    }

    CuingOverlayGroupWithSequence(String name,
            RunState type,
            CuingOverlayGroupManager manager,
            LinkedList<CuingOverlay> overlays) {
        super(name, type, manager, overlays);
        currentSequence = new CuingOverlayGroupSequenceQueueView(overlays);
    }

    public void setup() {
        currentSequence.reset();
    }
}

