package edu.umass.cs.binds.eyeframe;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

public class CuingOverlayGroupManager extends CuingOverlayGroup.Manager {
    private LinkedHashSet<CuingOverlayGroup> groups;
    private HashSet<CuingOverlay> overlays;
    private CuingOverlayGroup defaultGroup;

    CuingOverlayGroupManager() {
        this.groups = new LinkedHashSet<>();
        this.overlays = new HashSet<>();
        this.defaultGroup = null;
    }

    public void setup() {
        for(CuingOverlayGroup group : groups) {
            group.setup();
        }
    }

    protected void setDefaultGroup(CuingOverlayGroup group) {
        this.defaultGroup = group;
    }

    public CuingOverlayGroup getDefaultGroup() {
        return defaultGroup;
    }

    protected void addGroup(CuingOverlayGroup group) {
        groups.add(group);
    }

    protected void addOverlay(CuingOverlay overlay) {
        overlays.add(overlay);
    }

    protected void removeOverlay(CuingOverlay overlay) {
        overlays.remove(overlay);
    }

    protected void removeGroup(CuingOverlayGroup group) {
        groups.remove(group);
    }

    public HashSet<CuingOverlay> getListOfOverlays() {
        return overlays;
    }

    public LinkedHashSet<CuingOverlayGroup> getListOfGroups() {
        return groups;
    }

    public void disableAlerting() {
        for(CuingOverlayGroup group : groups) {
            group.disableAlerting();
        }
    }

    public void remove() {
        for(CuingOverlayGroup group : groups) {
            group.remove_();
        }
        this.groups = null;
        this.overlays = null;
    }

    public boolean groupNameIsTaken(String name) {
        for (CuingOverlayGroup group : groups) {
            if(group.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public boolean overlayNameIsTaken(String name) {
        for (CuingOverlay overlay : overlays) {
            if(overlay.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public boolean overlayNameIsTakenByOthers(CuingOverlay thisOverlay,
            String n) {
        for (CuingOverlay overlay : overlays) {
            if(overlay != thisOverlay && overlay.getName().equals(n)) {
                return true;
            }
        }
        return false;
    }


    public Map<String,Object> toMap() {
        HashMap<String,Object> variables = new HashMap<>(2);
        if(defaultGroup != null) {
            variables.put("defaultGroupName",defaultGroup.getName());
        } else {
            variables.put("defaultGroupName","");
        }
        ArrayList<Map<String,Object>> groupsMaps =
            new ArrayList<>(groups.size());
        for(CuingOverlayGroup group: groups) {
            groupsMaps.add(group.toMap());
        }
        variables.put("groups",groupsMaps);
        return variables;
    }

    public static CuingOverlayGroupManager toObject(
            Map<String,Object> map,
            PieGraph pieGraph,
            OverlayPieGraphLegend pieGraphLegend) throws Exception {
        CuingOverlayGroupManager manager = new CuingOverlayGroupManager();
        String defaultGroupName = (String) map.get("defaultGroupName");
        List<Map<String,Object>> groupsMaps =
            (List<Map<String,Object>>) map.get("groups");
        for(Map<String,Object> groupMap : groupsMaps) {
            CuingOverlayGroup.toObject(groupMap,manager,
                    pieGraph,pieGraphLegend,
                    defaultGroupName);
        }
        return manager;
    }
}
