package edu.umass.cs.binds.eyeframe;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;
import java.net.URL;

import javax.swing.border.Border;

import com.sun.jna.platform.WindowUtils;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef.HWND;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import java.lang.Number;

import java.awt.event.HierarchyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.HierarchyEvent;
import java.awt.event.WindowEvent;
import com.sun.jna.Native;

public abstract class ClickThrough {

    protected Stage stage;
    protected Group group;
    protected Scene scene;
    protected Rectangle rectangle;

    private static JFXPanel jfxPanel;
    private static Stage ownerStage;

    protected final static Color almostTransparentColor =
        Color.color(1.0,1.0,1.0,0.1);

    static {
        jfxPanel = new JFXPanel();
        createHiddenOwnerStage();
    }

    public static void setOwnerStage(Stage stage) {
        ownerStage = stage;
    }

    public static void createHiddenOwnerStage() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Dimension screen = Toolkit.getDefaultToolkit()
                    .getScreenSize();
                Group group = new Group();
                Scene scene = new Scene(group);
                scene.setFill(Color.TRANSPARENT);
                Stage stage = new Stage();
                stage.initStyle(StageStyle.TRANSPARENT);
                stage.setScene(scene);
                stage.setWidth(0.0);
                stage.setHeight(0.0);
                stage.setX(screen.getWidth()*2);
                stage.setY(screen.getHeight()*2);
                stage.setAlwaysOnTop(true);
                stage.show();
                setOwnerStage(stage);
            }
        });
    }

    ClickThrough(double x, double y, double height, double width) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                rectangle = new Rectangle();
                if(SettingsPanel.isBorder) {
                    rectangle.setFill(almostTransparentColor);
                    rectangle.setStroke(Color.WHITE);
                    rectangle.setStrokeWidth(SettingsPanel.borderThickness);
                } else {
                    rectangle.setFill(Color.WHITE);
                    rectangle.setStroke(Color.TRANSPARENT);
                    rectangle.setStrokeWidth(0.0);
                }
                rectangle.setStrokeType(StrokeType.INSIDE);
                group = new Group();
                group.getChildren().add(rectangle);
                scene = new Scene(group);
                scene.setFill(Color.TRANSPARENT);
                stage = new Stage();
                stage.initStyle(StageStyle.TRANSPARENT);
                stage.initOwner(ownerStage);
                stage.setScene(scene);
                stage.sizeToScene();
                stage.setAlwaysOnTop(true);
                setBounds(x, y, width, height);
                stage.widthProperty().addListener(
                        new ChangeListener<Number>() {
                            @Override
                            public void changed(
                                    ObservableValue<? extends Number> observable,
                                    Number oldValue,
                                    Number newValue) {
                                rectangle.setWidth(newValue.doubleValue());
                            }
                        });
                stage.heightProperty().addListener(
                        new ChangeListener<Number>() {
                            @Override
                            public void changed(
                                    ObservableValue<? extends Number> observable,
                                    Number oldValue,
                                    Number newValue) {
                                rectangle.setHeight(newValue.doubleValue());
                            }
                        });
            }
        });
    }

    protected void setBorder(double thickness, Color color) {
        rectangle.setStroke(color);
        rectangle.setStrokeWidth(thickness);
    }

    protected void setBackground(Color color) {
        rectangle.setFill(color);
    }

    public void setVisible(boolean b) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if(b) {
                    stage.show();
                } else {
                    stage.hide();
                }
            }
        });
    }

    public void setBounds(double x, double y, double width, double height) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                rectangle.setWidth(width);
                rectangle.setHeight(height);
                stage.setX(x);
                stage.setY(y);
                stage.sizeToScene();
            }
        });
    }

    public void setSize(double width, double height) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                rectangle.setWidth(width);
                rectangle.setHeight(height);
                stage.sizeToScene();
            }
        });
    }

    public void setLocation(double x, double y) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                stage.setX(x);
                stage.setY(y);
            }
        });
    }

    public void dispose() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                stage.close();
            }
        });
    }

    public void hide() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                stage.hide();
            }
        });
    }

    public void show() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                stage.show();
            }
        });
    }

    public void setColor(Color color) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if(SettingsPanel.isBorder) {
                    setBorder(SettingsPanel.borderThickness,
                            color);
                } else {
                    setBackground(color);
                }
            }
        });
    }
}
