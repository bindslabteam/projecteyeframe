package edu.umass.cs.binds.eyeframe;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;

public class Recorder implements IRecorder {
    private long startTime;
    private long gapTime;
    private ArrayList<String> listOfEvents =
        new ArrayList<String>();
    private SortedMap<String, String> firstTimes =
        new TreeMap<String, String>();
    private SortedMap<String, Integer> transitionCounts =
        new TreeMap<String, Integer>();
    private SortedMap<String, Double> totalTimes =
        new TreeMap<String, Double>();
    private SortedMap<String, Object[]> averageTimes =
        new TreeMap<String, Object[]>();
    private String lastLookedAt = null;

    Recorder() {
        //EDIT: initialized startTime
        startTime = System.nanoTime(); 
        gapTime = startTime;
    }

    public void addEvent(String event, String boxName, Boolean emptyEvent) {
        long currentTime = System.nanoTime();
        long interval    = (currentTime - startTime);
        long finish      = interval / 10000000;

        long ms      = finish % 100;
        long seconds = ((finish - ms) / 100) % 60;
        long minutes = (finish - (seconds*100)) / 6000;

        //This is the time between the last event and this event in ms
        double gapMs      = (currentTime - gapTime) / 1000000.0;

        //This is [minutes, seconds, ms] the time that the event happened
        String[] time = {Long.toString(minutes), Long.toString(seconds),
            Long.toString(ms)};

        //If one of the time units is in the single digits, add a '0' for clarity
        for(int i = 0; i<3; i++) {
            if (time[i].length()==1) {
                time[i] = "0" + time[i];
            }
        }

        String fullTime = time[0] + ":" + time[1] + ":" + time[2];

        listOfEvents.add(fullTime + "  " + event + boxName);

        //this will be used as "time since last event"
        gapTime = currentTime;

        String timeString = Double.toString((double)interval / 1000000.0);

        recordData(boxName, gapMs, timeString, emptyEvent);

        if(!emptyEvent){
            lastLookedAt = boxName;
        }
    }

    private void recordData(String boxName, double gap, String timeString,
            Boolean emptyEvent) {
        //emptyEvent == true means that a box has been looked *away* from.
        //So if it's false, we know that a box has been looked at.
        if(!emptyEvent){
            if(!firstTimes.containsValue(boxName) && boxName!=null){
                firstTimes.put(timeString, boxName);
            }

            //this will be true iff no box has been looked at before this
            if(lastLookedAt!=null){
                String transition = lastLookedAt + "-->" + boxName;
                if(transitionCounts.containsKey(transition)){
                    int tCount = transitionCounts.get(transition);
                    tCount++;
                    transitionCounts.put(transition, tCount);
                } else {
                    transitionCounts.put(transition, 1);
                }
            }
            //Since a box has been looked away from, we record how long was spent
            //looking at the last box we looked at
        } else {
            //if the box doesn't have a totalTime, it can be assumed that it
            //also lacks an averageTime
            if(!totalTimes.containsKey(lastLookedAt)){
                totalTimes.put(lastLookedAt, gap);
                averageTimes.put(lastLookedAt, new Object[]{gap, 1});
            }
            else {
                double total = totalTimes.get(lastLookedAt) + gap;
                totalTimes.put(lastLookedAt, total);
                Object[] average = averageTimes.get(lastLookedAt);
                int count = (int) average[1] + 1;
                averageTimes.put(lastLookedAt,  new Object[]{total / count,
                    count} );
            }

        }
    }

    public void saveEvents() {
        JFileChooser chooser = new JFileChooser();
        File userHome = new File(System.getProperty("user.home"));
        if (userHome.exists()) {
            chooser.setCurrentDirectory(userHome);
        }

        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(null,
                "Would You Like to Save the Recorded Data?",
                "EyeFrame Alert", dialogButton);
        if (dialogResult == JOptionPane.YES_OPTION) {
            try {
                File file = null;
                int returnVal = chooser.showSaveDialog(null);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    file = chooser.getSelectedFile();
                }
                String filename = file.getAbsolutePath();
                if (!filename.toLowerCase().endsWith(".txt")) {
                    filename = filename + ".txt";
                }

                if(file.isFile()) {
                    dialogResult = JOptionPane.showConfirmDialog(null,
                            "The file exists; Overwrite the current file?",
                            "Existing File", JOptionPane.YES_NO_OPTION);
                    if (dialogResult != JOptionPane.YES_OPTION) {
                        return;
                    }
                }

                PrintWriter out = new PrintWriter(filename);

                //print positions:
                out.println("---Positions---");

                SortedMap<String, ArrayList<String>> posInfo =
                    new TreeMap<String, ArrayList<String>>();

                for(CuingOverlay c : Controller.getListOfOverlays()){
                    String infoString = "(" + c.getPosX() + ", " + c.getPosY()
                        + ")" + ": " + c.getHeight() + "tall" + ","
                        + c.getWidth() + " wide";

                    if(!posInfo.containsKey(c.getName())){
                        posInfo.put(c.getName(), new ArrayList<String>());
                    }

                    posInfo.get(c.getName()).add(infoString);
                }

                for(String key : posInfo.keySet()){
                    out.println(key + ":");
                    for(String s : posInfo.get(key)){
                        out.println(s);
                    }

                    out.println();
                }

                out.println();



                //print firstTimes
                out.println("First Time Area Was Seen (in ms):");
                for (String key: firstTimes.keySet()){
                    out.println(firstTimes.get(key) + ": " + key);
                }

                out.println();
                out.println();

                //print transitionCounts
                out.println("Transition Counts:");
                for (String key: transitionCounts.keySet()){
                    out.println(key + ": " + transitionCounts.get(key));
                }

                out.println();
                out.println();

                //print totalTimes
                out.println("Total View Times (in ms):");
                for (String key: totalTimes.keySet()){
                    out.println(key + ": " + totalTimes.get(key));
                }

                out.println();
                out.println();

                //print averageTimes
                out.println("Average View Times (in ms):");
                for (String key: averageTimes.keySet()){
                    out.println(key + ": " + averageTimes.get(key)[0]);
                }

                out.println();
                out.println();

                //print raw event list
                out.println("Raw Data:");
                for(String e : listOfEvents){
                    out.println(e);
                }
                out.close();

            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (NullPointerException r) {
            }

        }

        for (CuingOverlay ct : Controller.getListOfOverlays()) {
            ct.setVisible(true);
        }
    }
}
