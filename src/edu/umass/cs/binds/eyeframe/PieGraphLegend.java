package edu.umass.cs.binds.eyeframe;

import javax.swing.Box;
//import javax.swing.Box.Filler;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import java.util.ArrayList;
import java.awt.Dimension;
import java.awt.Component;

import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
//import javax.swing.JSpinner.NumberEditor;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableColumnModel;

import javafx.scene.paint.Color;

import javax.swing.DefaultCellEditor;
import javax.swing.JScrollPane;
import javax.swing.SpinnerNumberModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

class PieGraphLegend<T extends PieGraphLegend.Slice>
        extends AbstractImprovedTableModel<T> {
    protected Dimension colorBoxDimensions;

    /**
     * @wbp.parser.constructor
     */
    PieGraphLegend() {
        this(new ArrayList<T>(),new Dimension(20,20));
    }

    PieGraphLegend(ArrayList<T> slices) {
        this(slices,new Dimension(20,20));
    }

    PieGraphLegend(ArrayList<T> slices,
            Dimension colorBoxDimensions) {
        this.columnNames = new String[] {
            "Overlay",
            "Value",
            "Weight %",
            "Delete ?"
        };
        this.rows = slices;
        this.colorBoxDimensions = colorBoxDimensions;
        this.table = new JTable(this);
        this.table.setBackground(new java.awt.Color(240,240,240));
        this.table.setDefaultRenderer(Color.class,
                new TableColorRenderer(false));
        TableColumnModel columnModel = this.table.getColumnModel();
        columnModel.getColumn(1).setCellEditor(new TableSpinnerEditor(
                    new SpinnerNumberModel(1.0,1.0,Double.MAX_VALUE,1.0)));
        columnModel.getColumn(2).setCellEditor(new TableSpinnerEditor(
                    new SpinnerNumberModel(1.0,1.0,100.0,1.0)));
        columnModel.getColumn(columnNames.length-1).setCellRenderer(
                new TableButtonRenderer("x"));
    }

    protected void setColumnNames(String[] columnNames) {
        if(columnNames[0] == this.columnNames[0]
                && columnNames[1] == this.columnNames[1]
                && columnNames[2] == this.columnNames[2]
                && columnNames[columnNames.length-1]
                == this.columnNames[this.columnNames.length-1]) {
            TableColumnModel columnModel = this.table.getColumnModel();
            columnModel.getColumn(this.columnNames.length-1).setCellRenderer(
                    null);
            this.columnNames = columnNames;
            this.fireTableStructureChanged();
            columnModel.getColumn(columnNames.length-1).setCellRenderer(
                    new TableButtonRenderer("x"));
            if(scrollPane != null) {
                fitHeader();
            }
        } else {
            //TODO: throw error, first 3 and last columns are reserved
        }
    }

    public void setSlices(ArrayList<T> slices) {
        int i = 0;
        for(T slice : slices) {
            i++;
        }
        setRows(slices);
    }

    protected void addSlice(T slice) {
        addRow(slice);
    }

    protected void removeSlice(T slice) {
        removeRow(slice);
    }

    protected T getSlice(int row) {
        return getRow(row);
    }

    protected int getSliceRow(T slice) {
        return this.rows.indexOf(slice);
    }

    public boolean isCellEditable(int row, int col) {
        if (col == 1 || col == 2) {
            return true;
        } else {
            return false;
        }
    }

    public Object getValueAt(int row, int col) {
        T slice = rows.get(row);
        if(col == 0) {
            return slice.getColor();
        } else if(col == 1) {
            return slice.getValue();
        } else if(col == 2) {
            return slice.getWeight() * 100.0;
        } else if(col == columnNames.length-1) {
            return new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    slice.delete();
                    fireTableRowsDeleted(row,row);
                }
            };
        }
        throw new RuntimeException("Missing value for row and column");
    }

    public void setValueAt(Object value, int row, int col) {
        T slice = rows.get(row);
        if(col == 1) {
            slice.setValue((double) value);
        } else if(col == 2) {
            slice.setWeight((double) value / 100.0);
        }
        slice.refreshPieGraph();
    }

    public static class Slice extends PieGraph.Slice {
        protected PieGraphLegend legend;

        Slice(double value, Color color, PieGraph pieGraph,
                PieGraphLegend legend) {
            super(value,color,pieGraph);
            this.legend = legend;
            this.legend.addSlice(this);
        }

        public void setAngleSize(double angleSize) {
            super.setAngleSize(angleSize);
            this.legend.fireTableCellUpdated(getLegendRow(), 1);
            this.legend.fireTableCellUpdated(getLegendRow(), 2);
        }

        public void delete() {
            remove();
        }

        public void remove() {
            super.remove();
            this.legend.removeSlice(this);
        }

        public void setValue(double v) {
            super.setValue(v);
            this.legend.fireTableCellUpdated(getLegendRow(), 1);
            this.legend.fireTableCellUpdated(getLegendRow(), 2);
        }

        public void setWeight(double v) {
            super.setWeight(v);
            this.legend.fireTableCellUpdated(getLegendRow(), 1);
            this.legend.fireTableCellUpdated(getLegendRow(), 2);
        }

        public int getLegendRow() {
            return this.legend.getSliceRow(this);
        }
    }
}
