package edu.umass.cs.binds.eyeframe;

import java.util.LinkedList;
import java.util.Map;

class CuingOverlayGroupRigidTriggerSequenceStrictInOrder extends CuingOverlayGroupWithTriggerSequence {
    private CuingOverlay lastOverlay;

    CuingOverlayGroupRigidTriggerSequenceStrictInOrder(String name,
            CuingOverlayGroupManager manager,
            int triggerSequencePos) {
        super(name,
                RunState.RIGID_TRIGGER_SEQUENCE_STRICT_OUTOFORDER,
                manager,
                triggerSequencePos);
        lastOverlay = null;
    }

    public void setup() {
        super.setup();
        lastOverlay = null;
    }

    public void focusedOnOverlay(CuingOverlay overlay,
            CuingOverlay previousOverlay, Stack stack) {
        if(stack.getCurrentSequenceGroup() == this) {
            if(overlay == currentSequence.first()) {
                this.lastOverlay = currentSequence.remove();
            }
        } else if(overlay == currentSequence.first()
                && !stack.isASequenceGroup(this)) {
            if(this.lastOverlay == previousOverlay
                    && overlay == stack.getMostNeglected()) {
                if(overlays.size()-currentSequence.size() == triggerSequencePos) {
                    currentSequence.remove();
                    stack.addGroupSequence(this,currentSequence);
                } else {
                    this.lastOverlay = currentSequence.remove();
                }
            } else {
                setup();
            }
        }
    }
}

