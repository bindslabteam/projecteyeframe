package edu.umass.cs.binds.eyeframe;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.geom.Point2D;
import javax.swing.JFrame;

import javax.swing.JOptionPane;

public class EyeTrackerSetup extends Coordinates {
    private String hostname;
    private int port;
    private Socket socketClient;
    private DataOutputStream outToServer;
    private BufferedReader stdIn;

    EyeTrackerSetup(Stack stack,
            CuingOverlayGroupManager cuingOverlayGroupManager) {
        this(stack, cuingOverlayGroupManager, "localhost", 4242);
    }

    EyeTrackerSetup(Stack stack,
            CuingOverlayGroupManager cuingOverlayGroupManager,
            String hostname, int port) {
        super(stack,cuingOverlayGroupManager);
        this.hostname = hostname;
        this.port = port;
    }

    public void run() {
        if(setup()) {
            Thread thisThread = Thread.currentThread();
            Point2D location;
            while(currentThread == thisThread) {
                location = getLocation();
                if(location != null) {
                    isInWindow(location);
                }
            }
            cleanup();
        }
    }

    protected boolean setup() {
        super.setup();
        try {
            socketClient = new Socket(hostname, port);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
                    "Unable to Establish Connection with the EyeTracker." +
                    " The Host is unknown." +
                    " Make sure the EyeTracker is connected and setup.");
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
                    "Unable to send or recieve data from EyeTracker.");
            return false;
        }
        for (CuingOverlay ct : Controller.getListOfOverlays()) {
            ct.setVisible(false);
        }
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult1 = JOptionPane.showConfirmDialog(null,
                "Would You Like to Calibrate the EyeTracker?",
                "EyeFrame Alert", dialogButton);
        if (dialogResult1 == JOptionPane.YES_OPTION) {
            try {
                calibrate();
            } catch (IOException e) {
                e.printStackTrace();
                JFrame frame = new JFrame();
                JOptionPane.showMessageDialog(frame,
                        "Calibration Failed." +
                        " Not able to send data to EyeTracker.");
                return false;
            }
        }

        for (CuingOverlay ct : Controller.getListOfOverlays()) {
            ct.setVisible(true);
        }
        try {
            this.sendData();
        } catch (IOException e) {
            e.printStackTrace();
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
                    "Failed to connect and initialize connection with" +
                    " EyeTracker");
            return false;
        }
        try {
            stdIn = new BufferedReader(new InputStreamReader(
                        socketClient.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
                    "Not able to recieve data from EyeTracker");
            return false;
        }
        return true;
    }

    protected Point2D getLocation() {
        try {
            String userInput = stdIn.readLine();
            if(userInput == null) {
                Controller.controller.stop();
            } else {
                int xIndex = userInput.indexOf("BPOGX=");
                if (xIndex > -1) {
                    String x = userInput.substring(xIndex + 7, xIndex + 14);
                    int yIndex = userInput.indexOf("BPOGY=");
                    String y = userInput.substring(yIndex + 7, yIndex + 14);
                    double xCoor = Double.parseDouble(x);
                    double yCoor = Double.parseDouble(y);
                    Dimension screenSize = Toolkit.getDefaultToolkit()
                        .getScreenSize();
                    double screenX = screenSize.getWidth();
                    double screenY = screenSize.getHeight();
                    return new Point2D.Double(xCoor * screenX,yCoor * screenY);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
                    "No longer able to recieve data from EyeTracker");
            Controller.controller.stop();
        }
        return null;
    }

    private void sendData() throws UnknownHostException, IOException {
        // connect to Eyetracker
        OutputStream os = socketClient.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);

        // once a connection has been established, send data to it
        String sendMessage = "<SET ID=\"ENABLE_SEND_DATA\" STATE=\"1\" />\r\n";
        bw.write(sendMessage);
        String sendMessages = "<SET ID=\"ENABLE_SEND_POG_BEST\" STATE=\"1\" />\r\n";
        bw.write(sendMessages);
        bw.flush();

    }

    public void calibrate() throws UnknownHostException, IOException {
        // TODO Auto-generated method stub
        OutputStream os = socketClient.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);

        String sendMessage = "<SET ID=\"ENABLE_SEND_DATA\" STATE=\"1\" />\r\n";
        bw.write(sendMessage);
        String sendMessages = "<SET ID=\"CALIBRATE_START\" STATE=\"1\" />\r\n";
        bw.write(sendMessages);
        sendMessages = "<SET ID=\"CALIBRATE_SHOW\" STATE=\"1\" />\r\n";
        bw.write(sendMessages);
        bw.flush();
    }
}
