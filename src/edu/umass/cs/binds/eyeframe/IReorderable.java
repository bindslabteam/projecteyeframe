package edu.umass.cs.binds.eyeframe;

public interface IReorderable {
   public void reorder(int fromIndex, int toIndex);
}
