package edu.umass.cs.binds.eyeframe;

import javax.swing.AbstractCellEditor;
import javax.swing.table.TableCellEditor;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JTable;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JSpinner;
import javax.swing.AbstractSpinnerModel;

public class TableSpinnerEditor extends AbstractCellEditor
        implements TableCellEditor {
    JSpinner spinner;
    AbstractSpinnerModel spinnerModel;
    protected static final String EDIT = "edit";

    public TableSpinnerEditor(AbstractSpinnerModel spinnerModel) {
        //Set up the editor (from the table's point of view),
        this.spinnerModel = spinnerModel;
        //new SpinnerNumberModel(1,1,Integer.MAX_VALUE,1)
        this.spinner = new JSpinner(spinnerModel);
    }

    //Implement the one CellEditor method that AbstractCellEditor doesn't.
    public Object getCellEditorValue() {
        return spinner.getValue();
    }

    //Implement the one method defined by TableCellEditor.
    public Component getTableCellEditorComponent(JTable table,
            Object value,
            boolean isSelected,
            int row,
            int column) {
        spinner.setValue(value);
        return spinner;
    }
}
