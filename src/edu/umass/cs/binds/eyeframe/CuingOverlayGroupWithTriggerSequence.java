package edu.umass.cs.binds.eyeframe;

import java.util.Map;

class CuingOverlayGroupWithTriggerSequence extends CuingOverlayGroupWithSequence {
    protected int triggerSequencePos;

    CuingOverlayGroupWithTriggerSequence(String name,
            RunState type,
            CuingOverlayGroupManager manager,
            int triggerSequencePos) {
        super(name,
                type,
                manager);
        this.triggerSequencePos = triggerSequencePos;
    }

    public void setTriggerSequencePos(int pos) {
        triggerSequencePos = pos;
    }

    public int getTriggerSequencePos() {
        return triggerSequencePos;
    }

    public Map<String,Object> toMap() {
        Map<String,Object> variables = super.toMap();
        variables.put("triggerSequencePos",triggerSequencePos);
        return variables;
    }
}

