package edu.umass.cs.binds.eyeframe;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

public class StopButton extends JWindow {
    protected JButton button;
    protected JWindow theJWindow;
    protected int posX = 1200;
    protected int posY = 50;
    transient private MouseAdapter mouseListener;
    transient private MouseAdapter mouseMotionListener;
    transient private MouseAdapter mouseListenerStopRun;

    // creates the stop button
    public StopButton() {
        theJWindow = this;
        button = new JButton(new ImageIcon(
                    StopButton.class.getResource("/images/stop.png")));
        this.add(button);
        this.mouseListenerStopRun = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                Controller.controller.stop();
            }
        };
        this.mouseListener = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    posX = e.getX();
                    posY = e.getY();
                }
            }

            public void mouseReleased(MouseEvent me) {
                posX = me.getXOnScreen() - posX;
                posY = me.getYOnScreen() - posY;
            }
        };
        this.mouseMotionListener = new MouseAdapter() {
            public void mouseDragged(MouseEvent evt) {
                theJWindow.setLocation(evt.getXOnScreen() - posX,
                        evt.getYOnScreen() - posY);
            }
        };
        eyeFrameRunStopped();
        this.setBackground(new Color(255, 0, 0, 224));
        this.setBounds(1200, 50, 50, 50);
        this.setAlwaysOnTop(true);
        this.setVisible(true);
    }

    protected void eyeFrameRunStarted() {
        button.removeMouseListener(this.mouseListener);
        button.removeMouseMotionListener(this.mouseMotionListener);
        button.addMouseListener(this.mouseListenerStopRun);
        button.setEnabled(true);
    }

    protected void eyeFrameRunStopped() {
        button.removeMouseListener(this.mouseListenerStopRun);
        button.setEnabled(false);
        button.addMouseListener(this.mouseListener);
        button.addMouseMotionListener(this.mouseMotionListener);
    }
}
