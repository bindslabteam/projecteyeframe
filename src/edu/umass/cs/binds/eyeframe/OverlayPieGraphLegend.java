package edu.umass.cs.binds.eyeframe;

import javax.swing.Box;
//import javax.swing.Box.Filler;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.awt.Dimension;
import javax.swing.SpinnerNumberModel;
import javax.swing.JSpinner;
//import javax.swing.JSpinner.NumberEditor;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableColumnModel;
import javax.swing.JOptionPane;
import javax.swing.DropMode;

import javafx.scene.paint.Color;

import javax.swing.DefaultCellEditor;
import javax.swing.JScrollPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Component;
import javax.swing.border.Border;
import java.text.NumberFormat;

import java.util.ArrayDeque;
import java.util.Deque;


class OverlayPieGraphLegend
        extends PieGraphLegend<OverlayPieGraphLegend.Slice>
        implements IReorderable {
    protected Deque<Color> availablePieSlicecolors;
    protected CuingOverlayGroup cuingOverlayGroup;

    /**
     * @wbp.parser.constructor
     */
    OverlayPieGraphLegend() {
        super();
        cuingOverlayGroup = null;
        setColumnNames(new String[] {
            "Overlay",
            "Value",
            "Weight %",
            "Focus Delay (msec.)",
            "Name",
            "Delete ?"
        });
        TableColumnModel columnModel = this.table.getColumnModel();
        columnModel.getColumn(3).setCellEditor(new TableSpinnerEditor(
                    new SpinnerNumberModel(new Long(0L),new Long(0L),new Long(10000000L),new Long(100L))));

        this.table.setDragEnabled(true);
        this.table.setDropMode(DropMode.INSERT_ROWS);
        this.table.setTransferHandler(new TableRowTransferHandler(this.table));

        availablePieSlicecolors = new ArrayDeque<Color>(12);
        availablePieSlicecolors.push(Color.ALICEBLUE);
        availablePieSlicecolors.push(Color.ANTIQUEWHITE);
        availablePieSlicecolors.push(Color.AQUA);
        availablePieSlicecolors.push(Color.AQUAMARINE);
        availablePieSlicecolors.push(Color.AZURE);
        availablePieSlicecolors.push(Color.BEIGE);
        availablePieSlicecolors.push(Color.BISQUE);
        availablePieSlicecolors.push(Color.BLACK);
        availablePieSlicecolors.push(Color.BLANCHEDALMOND);
        availablePieSlicecolors.push(Color.BLUE);
        availablePieSlicecolors.push(Color.BLUEVIOLET);
        availablePieSlicecolors.push(Color.BROWN);
        availablePieSlicecolors.push(Color.BURLYWOOD);
        availablePieSlicecolors.push(Color.CADETBLUE);
        availablePieSlicecolors.push(Color.CHARTREUSE);
        availablePieSlicecolors.push(Color.CHOCOLATE);
        availablePieSlicecolors.push(Color.CORAL);
        availablePieSlicecolors.push(Color.CORNFLOWERBLUE);
        availablePieSlicecolors.push(Color.CORNSILK);
        availablePieSlicecolors.push(Color.CRIMSON);
        availablePieSlicecolors.push(Color.CYAN);
        availablePieSlicecolors.push(Color.DARKBLUE);
        availablePieSlicecolors.push(Color.DARKCYAN);
        availablePieSlicecolors.push(Color.DARKGOLDENROD);
        availablePieSlicecolors.push(Color.DARKGREEN);
        availablePieSlicecolors.push(Color.DARKGREY);
        availablePieSlicecolors.push(Color.DARKKHAKI);
        availablePieSlicecolors.push(Color.DARKMAGENTA);
        availablePieSlicecolors.push(Color.DARKOLIVEGREEN);
        availablePieSlicecolors.push(Color.DARKORANGE);
        availablePieSlicecolors.push(Color.DARKORCHID);
        availablePieSlicecolors.push(Color.DARKRED);
        availablePieSlicecolors.push(Color.DARKSALMON);
        availablePieSlicecolors.push(Color.DARKSEAGREEN);
        availablePieSlicecolors.push(Color.DARKSLATEBLUE);
        availablePieSlicecolors.push(Color.DARKSLATEGREY);
        availablePieSlicecolors.push(Color.DARKTURQUOISE);
        availablePieSlicecolors.push(Color.DARKVIOLET);
        availablePieSlicecolors.push(Color.DEEPPINK);
        availablePieSlicecolors.push(Color.DEEPSKYBLUE);
        availablePieSlicecolors.push(Color.DIMGREY);
        availablePieSlicecolors.push(Color.DODGERBLUE);
        availablePieSlicecolors.push(Color.FIREBRICK);
        availablePieSlicecolors.push(Color.FLORALWHITE);
        availablePieSlicecolors.push(Color.FORESTGREEN);
        availablePieSlicecolors.push(Color.FUCHSIA);
        availablePieSlicecolors.push(Color.GAINSBORO);
        availablePieSlicecolors.push(Color.GHOSTWHITE);
        availablePieSlicecolors.push(Color.GOLD);
        availablePieSlicecolors.push(Color.GOLDENROD);
        availablePieSlicecolors.push(Color.GREEN);
        availablePieSlicecolors.push(Color.GREENYELLOW);
        availablePieSlicecolors.push(Color.GREY);
        availablePieSlicecolors.push(Color.HONEYDEW);
        availablePieSlicecolors.push(Color.HOTPINK);
        availablePieSlicecolors.push(Color.INDIANRED);
        availablePieSlicecolors.push(Color.INDIGO);
        availablePieSlicecolors.push(Color.IVORY);
        availablePieSlicecolors.push(Color.KHAKI);
        availablePieSlicecolors.push(Color.LAVENDER);
        availablePieSlicecolors.push(Color.LAVENDERBLUSH);
        availablePieSlicecolors.push(Color.LAWNGREEN);
        availablePieSlicecolors.push(Color.LEMONCHIFFON);
        availablePieSlicecolors.push(Color.LIGHTBLUE);
        availablePieSlicecolors.push(Color.LIGHTCORAL);
        availablePieSlicecolors.push(Color.LIGHTCYAN);
        availablePieSlicecolors.push(Color.LIGHTGOLDENRODYELLOW);
        availablePieSlicecolors.push(Color.LIGHTGREEN);
        availablePieSlicecolors.push(Color.LIGHTGREY);
        availablePieSlicecolors.push(Color.LIGHTPINK);
        availablePieSlicecolors.push(Color.LIGHTSALMON);
        availablePieSlicecolors.push(Color.LIGHTSEAGREEN);
        availablePieSlicecolors.push(Color.LIGHTSKYBLUE);
        availablePieSlicecolors.push(Color.LIGHTSLATEGREY);
        availablePieSlicecolors.push(Color.LIGHTSTEELBLUE);
        availablePieSlicecolors.push(Color.LIGHTYELLOW);
        availablePieSlicecolors.push(Color.LIME);
        availablePieSlicecolors.push(Color.LIMEGREEN);
        availablePieSlicecolors.push(Color.LINEN);
        availablePieSlicecolors.push(Color.MAGENTA);
        availablePieSlicecolors.push(Color.MAROON);
        availablePieSlicecolors.push(Color.MEDIUMAQUAMARINE);
        availablePieSlicecolors.push(Color.MEDIUMBLUE);
        availablePieSlicecolors.push(Color.MEDIUMORCHID);
        availablePieSlicecolors.push(Color.MEDIUMPURPLE);
        availablePieSlicecolors.push(Color.MEDIUMSEAGREEN);
        availablePieSlicecolors.push(Color.MEDIUMSLATEBLUE);
        availablePieSlicecolors.push(Color.MEDIUMSPRINGGREEN);
        availablePieSlicecolors.push(Color.MEDIUMTURQUOISE);
        availablePieSlicecolors.push(Color.MEDIUMVIOLETRED);
        availablePieSlicecolors.push(Color.MIDNIGHTBLUE);
        availablePieSlicecolors.push(Color.MINTCREAM);
        availablePieSlicecolors.push(Color.MISTYROSE);
        availablePieSlicecolors.push(Color.MOCCASIN);
        availablePieSlicecolors.push(Color.NAVAJOWHITE);
        availablePieSlicecolors.push(Color.NAVY);
        availablePieSlicecolors.push(Color.OLDLACE);
        availablePieSlicecolors.push(Color.OLIVE);
        availablePieSlicecolors.push(Color.OLIVEDRAB);
        availablePieSlicecolors.push(Color.ORANGE);
        availablePieSlicecolors.push(Color.ORANGERED);
        availablePieSlicecolors.push(Color.ORCHID);
        availablePieSlicecolors.push(Color.PALEGOLDENROD);
        availablePieSlicecolors.push(Color.PALEGREEN);
        availablePieSlicecolors.push(Color.PALETURQUOISE);
        availablePieSlicecolors.push(Color.PALEVIOLETRED);
        availablePieSlicecolors.push(Color.PAPAYAWHIP);
        availablePieSlicecolors.push(Color.PEACHPUFF);
        availablePieSlicecolors.push(Color.PERU);
        availablePieSlicecolors.push(Color.PINK);
        availablePieSlicecolors.push(Color.PLUM);
        availablePieSlicecolors.push(Color.POWDERBLUE);
        availablePieSlicecolors.push(Color.PURPLE);
        availablePieSlicecolors.push(Color.RED);
        availablePieSlicecolors.push(Color.ROSYBROWN);
        availablePieSlicecolors.push(Color.ROYALBLUE);
        availablePieSlicecolors.push(Color.SADDLEBROWN);
        availablePieSlicecolors.push(Color.SALMON);
        availablePieSlicecolors.push(Color.SANDYBROWN);
        availablePieSlicecolors.push(Color.SEAGREEN);
        availablePieSlicecolors.push(Color.SEASHELL);
        availablePieSlicecolors.push(Color.SIENNA);
        availablePieSlicecolors.push(Color.SILVER);
        availablePieSlicecolors.push(Color.SKYBLUE);
        availablePieSlicecolors.push(Color.SLATEBLUE);
        availablePieSlicecolors.push(Color.SPRINGGREEN);
        availablePieSlicecolors.push(Color.STEELBLUE);
        availablePieSlicecolors.push(Color.TAN);
        availablePieSlicecolors.push(Color.TEAL);
        availablePieSlicecolors.push(Color.THISTLE);
        availablePieSlicecolors.push(Color.TOMATO);
        availablePieSlicecolors.push(Color.TURQUOISE);
        availablePieSlicecolors.push(Color.VIOLET);
        availablePieSlicecolors.push(Color.WHEAT);
        availablePieSlicecolors.push(Color.WHITE);
        availablePieSlicecolors.push(Color.YELLOW);
        availablePieSlicecolors.push(Color.YELLOWGREEN);
    }

    public void initScrollPane() {
        super.initScrollPane();
        this.scrollPane.setPreferredSize(new Dimension(
                this.scrollPane.getPreferredSize().width,
                500));
    }

    public void setScrollPane(
            JScrollPane scrollPane,
            JPanel scrollPaneJPanel) {
        super.setScrollPane(scrollPane,scrollPaneJPanel);
        this.scrollPane.setPreferredSize(new Dimension(
                this.scrollPane.getPreferredSize().width,
                500));
    }

    public Slice createSlice(double value,
            PieGraph pieGraph,
            CuingOverlay cuingOverlay) {
        return new Slice(value,pieGraph,this,cuingOverlay);
    }

    public Slice createSlice(double value, Color color,
            PieGraph pieGraph,
            CuingOverlay cuingOverlay) {
        return new Slice(value,color,pieGraph,this,cuingOverlay);
    }

    protected void addSlice(Slice slice, CuingOverlay cuingOverlay) {
        if(cuingOverlayGroup == null
                || cuingOverlay.getGroup() == cuingOverlayGroup) {
            super.addSlice(slice);
        }
    }

    protected void addSlice(Slice slice) {
    }

    protected Slice getSlice(int row) {
        return (Slice) super.getSlice(row);
    }

    public void setCuingOverlayGroup(CuingOverlayGroup group) {
        this.cuingOverlayGroup = group;
    }

    public void reorder(int fromIndex, int toIndex) {
        Slice slice = (OverlayPieGraphLegend.Slice) this.rows.remove(fromIndex);
        slice.getCuingOverlay().setGroupPosition(toIndex);
        this.rows.add(toIndex,slice);
    }

    public boolean isCellEditable(int row, int col) {
        if (col == 3 || col == 4) {
            return true;
        } else {
            return super.isCellEditable(row,col);
        }
    }

    public Object getValueAt(int row, int col) {
        if(col == 3) {
            return getSlice(row).getCuingOverlay()
                    .getMinimumFocusDelay();
        } else if(col == 4) {
            return getSlice(row).getCuingOverlay()
                    .getName();
        } else {
            return super.getValueAt(row,col);
        }
    }

    public void setValueAt(Object value, int row, int col) {
        if(col == 3) {
            getSlice(row).getCuingOverlay()
                .setMinimumFocusDelay((long)value);
        } else if(col == 4) {
            CuingOverlay cuingOverlay = getSlice(row).getCuingOverlay();
            if(cuingOverlay.nameIsTakenByOthers((String)value)) {
                JOptionPane.showMessageDialog(null,
                        "The Window Name is already taken. Choose a different window name.",
                        "EyeFrame Error", JOptionPane.INFORMATION_MESSAGE);
            } else {
                cuingOverlay
                    .setName(((String)value).trim());
            }
        } else {
            super.setValueAt(value,row,col);
        }
    }

    public static class Slice extends PieGraphLegend.Slice {
        protected CuingOverlay cuingOverlay;

        Slice(double value,
                PieGraph pieGraph, OverlayPieGraphLegend legend,
                CuingOverlay cuingOverlay) {
            super(value,legend.availablePieSlicecolors.pop(),pieGraph,legend);
            this.cuingOverlay = cuingOverlay;
            legend.addSlice(this,cuingOverlay);
        }

        Slice(double value, Color color,
                PieGraph pieGraph, OverlayPieGraphLegend legend,
                CuingOverlay cuingOverlay) {
            super(value,color,pieGraph,legend);
            legend.availablePieSlicecolors.remove(color);
            this.cuingOverlay = cuingOverlay;
            legend.addSlice(this,cuingOverlay);
        }

        public CuingOverlay getCuingOverlay() {
            return cuingOverlay;
        }

        public void delete() {
            cuingOverlay.remove();
        }

        public void remove() {
            super.remove();
            ((OverlayPieGraphLegend) legend).availablePieSlicecolors
                .push(getColor());
        }
    }
}

