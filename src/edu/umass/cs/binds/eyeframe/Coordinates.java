package edu.umass.cs.binds.eyeframe;

import java.awt.MouseInfo;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.awt.geom.Point2D;
import java.util.HashSet;
import java.util.HashMap;

abstract public class Coordinates implements Runnable {

    protected HashMap<CuingOverlay,Long> lastRunTime;
    protected HashSet<CuingOverlay> overlayWeightReset;
    protected volatile Thread currentThread;
    protected Stack stack;
    protected CuingOverlayGroupManager cuingOverlayGroupManager;
    protected CuingOverlay lastOverlay;

    Coordinates(Stack stack,
            CuingOverlayGroupManager cuingOverlayGroupManager) {
        this.cuingOverlayGroupManager = cuingOverlayGroupManager;
        this.stack = stack;
        lastRunTime = new HashMap<>();
        overlayWeightReset = new HashSet<>();
        lastOverlay = null;
    }

    protected abstract Point2D getLocation();

    public void start() {
        currentThread = new Thread(this);
        currentThread.start();
    }

    public void stop() {
        currentThread = null;
    }

    protected boolean setup() {
        cuingOverlayGroupManager.setup();
        stack.setup();
        lastOverlay = null;
        return true;
    }

    protected void cleanup() {
        stack.cleanup();
        stack = null;
        cuingOverlayGroupManager = null;
        lastOverlay = null;
    }

    public void run() {
        if(setup()) {
            Thread thisThread = Thread.currentThread();
            Point2D location;
            while(currentThread == thisThread) {
                location = getLocation();
                isInWindow(location);
            }
            cleanup();
        }
    }

    protected void isInWindow(Point2D location) {
        boolean updateOverlaysWeights = false;
        long curRunTime = System.currentTimeMillis();

        for (CuingOverlay ct : Controller.getListOfOverlays()) {
            if (ct.containsLocation(location.getX(),location.getY())) {
                if(lastRunTime.containsKey(ct)) {
                    if(!overlayWeightReset.contains(ct)
                            && curRunTime - lastRunTime.get(ct)
                                >= ct.getMinimumFocusDelay()) {
                        overlayWeightReset.add(ct);
                        updateOverlaysWeights = true;
                        stack.resetOverlayWeight(ct);
                        ct.focusedOnOverlay(lastOverlay,stack);
                        lastOverlay = ct;
                    }
                } else {
                    lastRunTime.put(ct, System.currentTimeMillis());
                    ct.enteredWindow();
                }
            } else if(lastRunTime.containsKey(ct)) {
                lastRunTime.remove(ct);
                if(overlayWeightReset.contains(ct)) {
                    overlayWeightReset.remove(ct);
                } else if(ct.getMinimumFocusDelay() == 0) {
                    updateOverlaysWeights = true;
                    stack.resetOverlayWeight(ct);
                    ct.focusedOnOverlay(lastOverlay,stack);
                    lastOverlay = ct;
                }
                ct.exitedWindow();
            }
        }
        if (updateOverlaysWeights) {
            stack.changeWeight();
        }
    }
}
