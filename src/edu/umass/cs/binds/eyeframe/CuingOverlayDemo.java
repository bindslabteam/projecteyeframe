package edu.umass.cs.binds.eyeframe;

import javafx.scene.paint.Color;
import javafx.application.Platform;

public class CuingOverlayDemo extends ClickThrough {
    // creates the settings window
    public CuingOverlayDemo(int x, int y) {
        super(x,y,200,200);
        setVisible(true);
    }

    protected void setBorder(double thickness, Color color) {
        super.setBorder(thickness,color);
        rectangle.setFill(Color.TRANSPARENT);
    }

    protected void setBackground(Color color) {
        super.setBackground(color);
        rectangle.setStroke(Color.TRANSPARENT);
        rectangle.setStrokeWidth(0.0);
    }
}
