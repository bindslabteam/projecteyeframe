package edu.umass.cs.binds.eyeframe;

interface IRecorder {
    public void addEvent(String event, String boxName, Boolean emptyEvent);
    public void saveEvents();
}
