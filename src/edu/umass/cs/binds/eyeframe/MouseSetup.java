package edu.umass.cs.binds.eyeframe;

import java.awt.MouseInfo;
import java.io.IOException;
import java.awt.geom.Point2D;

public class MouseSetup extends Coordinates {
    MouseSetup(Stack stack, CuingOverlayGroupManager cuingOverlayGroupManager) {
        super(stack,cuingOverlayGroupManager);
    }

    public Point2D getLocation() {
        return MouseInfo.getPointerInfo().getLocation();
    }
}
