package edu.umass.cs.binds.eyeframe;

import javax.swing.Box;
//import javax.swing.Box.Filler;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import java.util.ArrayList;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Component;

import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
//import javax.swing.JSpinner.NumberEditor;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableColumnModel;
import javax.swing.DefaultCellEditor;
import javax.swing.JScrollPane;
import javax.swing.SpinnerNumberModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

abstract class AbstractImprovedTableModel<T> extends AbstractTableModel {
    protected ArrayList<T> rows;
    protected JTable table;
    protected JScrollPane scrollPane;
    protected JPanel scrollPaneJPanel;
    protected String[] columnNames;

    public void initScrollPane() {
        scrollPaneJPanel = new JPanel();
        scrollPaneJPanel.setLayout(new BoxLayout(
                    scrollPaneJPanel, BoxLayout.Y_AXIS));
        scrollPaneJPanel.add(this.table);
        scrollPane = new JScrollPane(scrollPaneJPanel);
        scrollPane.setColumnHeaderView(this.table.getTableHeader());
        fitHeader();
    }

    public void setScrollPane(
            JScrollPane scrollPane,
            JPanel scrollPaneJPanel) {
        this.scrollPane = scrollPane;
        this.scrollPaneJPanel = scrollPaneJPanel;
        scrollPaneJPanel.add(this.table);
        fitHeader();
    }

    protected void fitHeader() {
        //this.table.setFillsViewportHeight(true);
        this.table.setRowHeight(((FontUIResource) UIManager
                    .get("Table.font")).getSize() + 8);
        //this.table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //this.table.setAutoscrolls(false);
        //this.table.getTableHeader().setResizingAllowed(false);
        //this.table.getTableHeader().setReorderingAllowed(false);
        TableColumnModel columnModel = table.getColumnModel();
        for (int col = 0; col < table.getColumnCount(); col++) {
            TableColumn column = columnModel.getColumn(col);
            TableCellRenderer headerRenderer = column.getHeaderRenderer();
            if (headerRenderer == null) {
                headerRenderer = table.getTableHeader().getDefaultRenderer();
            }
            Component headerComp = headerRenderer.getTableCellRendererComponent(table, column.getHeaderValue(), false, false, 0, col);
            // note some extra padding
            column.setPreferredWidth(headerComp.getPreferredSize().width + 6);//IntercellSpacing * 2 + 2 * 2 pixel instead of taking this value from Borders
        }
        DefaultTableCellRenderer stringRenderer = (DefaultTableCellRenderer) table.getDefaultRenderer(String.class);
        stringRenderer.setHorizontalAlignment(SwingConstants.CENTER);
        table.setPreferredScrollableViewportSize(new Dimension(table.getPreferredSize().width+5,scrollPane.getPreferredSize().height));
    }

    protected void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
        this.fireTableStructureChanged();
        if(scrollPane != null) {
            fitHeader();
        }
    }

    public JTable getTable() {
        return table;
    }

    public JScrollPane getScrollPane() {
        return scrollPane;
    }

    public JPanel getScrollPaneJPanel() {
        return scrollPaneJPanel;
    }

    protected void setRows(ArrayList<T> rows) {
        this.rows = rows;
        fireTableDataChanged();
    }

    protected void addRow(T row) {
        rows.add(row);
        fireTableDataChanged();
    }

    protected void removeRow(T row) {
        rows.remove(row);
        fireTableDataChanged();
    }

    protected T getRow(int row) {
        return rows.get(row);
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    public int getColumnCount() { return columnNames.length; }

    public int getRowCount() { return rows.size(); }
}

