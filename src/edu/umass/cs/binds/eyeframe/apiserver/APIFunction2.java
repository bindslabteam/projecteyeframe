package edu.umass.cs.binds.eyeframe.apiserver;

@FunctionalInterface
public interface APIFunction2<T0,T1,R> extends APIFunction<R> {

    @SuppressWarnings("unchecked")
    default R apply(Object... args) {
        return method((T0) args[0],(T1) args[1]);
    }

    R method(T0 arg0, T1 arg1);
}

