package edu.umass.cs.binds.eyeframe.apiserver;

@FunctionalInterface
public interface APIFunction5<T0,T1,T2,T3,T4,R> extends APIFunction<R> {

    @SuppressWarnings("unchecked")
    default R apply(Object... args) {
        return method(
                (T0) args[0],
                (T1) args[1],
                (T2) args[2],
                (T3) args[3],
                (T4) args[4]);
    }

    R method(
            T0 arg0,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4);
}

