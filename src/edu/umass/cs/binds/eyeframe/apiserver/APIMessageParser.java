package edu.umass.cs.binds.eyeframe.apiserver;

import edu.umass.cs.binds.eyeframe.IOConsumer;

import com.fasterxml.jackson.jr.ob.JSON;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import java.util.HashMap;
import java.util.EnumSet;
import java.util.ArrayList;
import java.nio.channels.SocketChannel;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.net.Socket;
import java.io.IOException;
import java.io.StringWriter;

class APIMessageParser {
    private SocketChannel socketChannel;
    private JsonFactory jsonFactory;
    private JsonParser jsonParser;
    private JsonGenerator jsonGenerator;
    private IOConsumer<JsonToken> stage;
    private String currentClassName;
    private String currentObjectName;
    private String currentFuncName;
    private APIObject apiObject;
    private APIFunctions<JsonGenerator> apiFunctions;
    private APIFunction<JsonGenerator> apiFunction;
    private ArrayList<Object> currentFuncArgs;
    private byte[] jsonMessage;
    private int jsonMessageSizeRemaining;
    private boolean writtenClassName;
    private boolean writtenObjectName;

    APIMessageParser(SocketChannel socketChannel) throws IOException {
        this.socketChannel = socketChannel;
        Socket socket = socketChannel.socket();
        this.jsonFactory = new JsonFactory();
        this.jsonMessageSizeRemaining = -1;
    }

    public void close() throws IOException {
        this.jsonParser.close();
        this.jsonGenerator.close();
        this.socketChannel.close();
    }

    public void processMessage(int bytesRead, ByteBuffer byteBuffer)
            throws IOException {
        byteBuffer.position(0);
        if(jsonMessageSizeRemaining == -1) {
            startMessage(bytesRead,byteBuffer);
        } else {
            continueMessage(bytesRead,byteBuffer);
        }
    }

    protected void continueMessage(int bufferRemainingSize,
            ByteBuffer byteBuffer) throws IOException {
        if(jsonMessageSizeRemaining <= bufferRemainingSize) {
            byte[] byteArray = new byte[jsonMessageSizeRemaining];
            byteBuffer.get(byteArray,0,jsonMessageSizeRemaining);
            byteBuffer.position(jsonMessageSizeRemaining);
            System.arraycopy(byteArray,0,
                    jsonMessage,jsonMessage.length - jsonMessageSizeRemaining,
                    jsonMessageSizeRemaining);
            parseMessage();
            if(jsonMessageSizeRemaining < bufferRemainingSize) {
                startMessage(bufferRemainingSize - jsonMessageSizeRemaining,
                        byteBuffer);
            }
            jsonMessageSizeRemaining = -1;
            jsonMessage = null;
        } else {
            byte[] byteArray = new byte[bufferRemainingSize];
            byteBuffer.get(byteArray);
            byteBuffer.position(bufferRemainingSize);
            System.arraycopy(byteArray,0,
                    jsonMessage,jsonMessage.length - jsonMessageSizeRemaining,
                    bufferRemainingSize);
        }
    }

    protected void startMessage(int bytesRead, ByteBuffer byteBuffer)
            throws IOException {
        jsonMessageSizeRemaining = byteBuffer.getInt();
        if(jsonMessageSizeRemaining > 0) {
            jsonMessage = new byte[jsonMessageSizeRemaining];
            if(bytesRead > byteBuffer.position()) {
                continueMessage(bytesRead - byteBuffer.position(), byteBuffer);
            }
        } else {
            //TODO: reply with error message
        }
    }

    protected void parseMessage() throws IOException {
        JsonToken event;
        String keyName;
        StringWriter stringWriter = new StringWriter();
        jsonParser = jsonFactory.createParser(
                new String(jsonMessage,
                    StandardCharsets.UTF_8));
        jsonGenerator = jsonFactory.createGenerator(stringWriter);
        writtenClassName = false;
        writtenObjectName = false;
        stage = (t) -> parseClassName1(t);
        event = jsonParser.nextToken();
        try {
            while(event != null) {
                stage.accept(event);
                event = jsonParser.nextToken();
            }
            messageCleanup();
        } catch(APIRuntimeException ex) {
            messageCleanup();
            jsonGenerator.writeObjectFieldStart("Error");
            if(currentClassName != null) {
                jsonGenerator.writeObjectFieldStart(currentClassName);
            }
            if(currentObjectName != null) {
                jsonGenerator.writeObjectFieldStart(currentObjectName);
            }
            if(currentFuncName != null) {
                jsonGenerator.writeObjectFieldStart(currentFuncName);
            }
            jsonGenerator.writeStringField("Message",ex.getMessage());
            if(currentFuncName != null) {
                jsonGenerator.writeEndObject();
            }
            if(currentObjectName != null) {
                jsonGenerator.writeEndObject();
            }
            if(currentClassName != null) {
                jsonGenerator.writeEndObject();
            }
            jsonGenerator.writeEndObject();
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.flush();
        socketChannel.write(
                ByteBuffer.wrap(
                    stringWriter
                        .toString()
                        .getBytes(StandardCharsets.UTF_8)));
    }

    protected void messageCleanup() throws IOException {
        if(writtenObjectName) {
            jsonGenerator.writeEndObject();
            writtenObjectName = false;
        }
        if(writtenClassName) {
            jsonGenerator.writeEndObject();
            writtenClassName = false;
        }
    }

    protected void parseClassName1(JsonToken event) throws IOException {
        if(event == JsonToken.START_OBJECT) {
            stage = (t) -> parseClassName2(t);
            jsonGenerator.writeStartObject();
        } else {
            //TODO: throw invalid syntax
            throw new APIRuntimeException("invalid syntax");
        }
    }

    protected void parseClassName2(JsonToken event) throws IOException {
        if(event == JsonToken.FIELD_NAME) {
            currentClassName = jsonParser.getText();
            stage = (t) -> parseObjectOrFunctionName1(t);
        } else if(event == JsonToken.END_OBJECT) {
            currentClassName = null;
            apiFunctions = null;
            stage = (t) -> parseClassName1(t);
            jsonGenerator.writeEndObject();
        } else {
            //TODO: throw invalid syntax
            throw new APIRuntimeException("invalid syntax");
        }
    }

    protected void parseObjectOrFunctionName1(JsonToken event) {
        if(event == JsonToken.START_OBJECT) {
            stage = (t) -> parseObjectOrFunctionName2(t);
        } else {
            //TODO: throw invalid syntax
            throw new APIRuntimeException("invalid syntax");
        }
    }

    protected void parseObjectOrFunctionName2(JsonToken event)
            throws IOException {
        if(event == JsonToken.FIELD_NAME) {
            currentObjectName = jsonParser.getText();
            stage = (t) -> parseObjectOrFunctionName3(t);
        } else if(event == JsonToken.END_OBJECT) {
            currentClassName = null;
            apiFunctions = null;
            stage = (t) -> parseClassName2(t);
            if(writtenClassName) {
                jsonGenerator.writeEndObject();
                writtenClassName = false;
            }
        } else {
            //TODO: throw invalid syntax
            throw new APIRuntimeException("invalid syntax");
        }
    }

    protected void parseObjectOrFunctionName3(JsonToken event)
                throws IOException {
        if(event == JsonToken.START_OBJECT) {
            currentFuncName = null;
            apiObject = APIConfiguration.getAPIObject(currentClassName);
            if(apiObject == null) {
                //jsonParser.skipChildren();
                //TODO: throw invalid class name
                throw new APIRuntimeException("invalid class name");
            }
            apiFunctions = apiObject.get(currentObjectName);
            if(apiFunctions == null) {
                //jsonParser.skipChildren();
                //TODO: throw invalid object name
                throw new APIRuntimeException("invalid object name");
            }
            apiFunction = null;
            stage = (t) -> parseFunctionName(t);
        } else if(event == JsonToken.START_ARRAY) {
            currentFuncName = currentObjectName;
            currentObjectName = null;
            apiFunctions = APIConfiguration.getAPIFunctions(currentClassName);
            if(apiFunctions == null) {
                //jsonParser.skipChildren();
                //TODO: throw invalid class name
                throw new APIRuntimeException("invalid class name");
            }
            apiFunction = apiFunctions.get(currentFuncName);
            if(apiFunction == null) {
                throw new APIRuntimeException("invalid function name");
            }
            currentFuncArgs = new ArrayList<>();
            stage = (t) -> parseFunctionArgs2(t);
        } else {
            //TODO: throw invalid syntax
            throw new APIRuntimeException("invalid syntax");
        }
    }

    protected void parseFunctionArgs1(JsonToken event) {
        if(event == JsonToken.START_ARRAY) {
            currentFuncArgs = new ArrayList<>();
            stage = (t) -> parseFunctionArgs2(t);
        } else {
            //TODO: throw invalid syntax
            throw new APIRuntimeException("invalid syntax");
        }
    }

    protected void parseFunctionArgs2(JsonToken event)
            throws IOException {
        if(event == JsonToken.VALUE_TRUE
                || event == JsonToken.VALUE_FALSE) {
            currentFuncArgs.add(jsonParser.getBooleanValue());
            stage = (t) -> parseFunctionArgs2(t);
        } else if(event == JsonToken.VALUE_NUMBER_FLOAT
                || event == JsonToken.VALUE_NUMBER_INT) {
            currentFuncArgs.add(jsonParser.getNumberValue());
            stage = (t) -> parseFunctionArgs2(t);
        } else if(event == JsonToken.VALUE_STRING) {
            currentFuncArgs.add(jsonParser.getText());
            stage = (t) -> parseFunctionArgs2(t);
        } else if(event == JsonToken.VALUE_NULL) {
            currentFuncArgs.add(null);
            stage = (t) -> parseFunctionArgs2(t);
        } else if(event == JsonToken.END_ARRAY) {
            JsonGenerator message = apiFunction.apply(
                    currentFuncArgs.toArray());
            if(message != null) {
                if(!writtenClassName) {
                    jsonGenerator
                        .writeObjectFieldStart(currentClassName);
                }
                if(!writtenObjectName && currentObjectName != null) {
                    jsonGenerator
                        .writeObjectFieldStart(currentObjectName);
                }
                jsonGenerator.writeFieldName(currentFuncName);
                    jsonGenerator.writeRawValue(message.toString());
                message.close();
            }
            currentFuncName = null;
            apiFunction = null;
            currentFuncArgs = null;
            stage = (t) -> parseFunctionName(t);
        } else {
            //TODO: throw invalid syntax
            throw new APIRuntimeException("invalid syntax");
        }
    }

    protected void parseFunctionName(JsonToken event)
            throws IOException {
        if(event == JsonToken.FIELD_NAME) {
            currentFuncName = jsonParser.getText();
            apiFunction = apiFunctions.get(currentFuncName);
            if(apiFunction == null) {
                throw new APIRuntimeException("invalid function name");
            }
            stage = (t) -> parseFunctionArgs1(t);
        } else if(event == JsonToken.END_OBJECT) {
            if(currentObjectName == null) {
                currentClassName = null;
                apiFunctions = null;
                stage = (t) -> parseClassName2(t);
                if(writtenClassName) {
                    jsonGenerator.writeEndObject();
                    writtenClassName = false;
                }
            } else {
                currentObjectName = null;
                apiFunctions = null;
                apiObject = null;
                stage = (t) -> parseObjectOrFunctionName2(t);
                if(writtenObjectName) {
                    jsonGenerator.writeEndObject();
                    writtenObjectName = false;
                }
            }
        } else {
            //TODO: throw invalid syntax
            throw new APIRuntimeException("invalid syntax");
        }
    }
}
