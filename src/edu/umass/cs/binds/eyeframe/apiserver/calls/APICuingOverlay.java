package edu.umass.cs.binds.eyeframe.apiserver.calls;

import edu.umass.cs.binds.eyeframe.Controller;
import edu.umass.cs.binds.eyeframe.CuingOverlay;
import edu.umass.cs.binds.eyeframe.apiserver.APIFunctions;
import edu.umass.cs.binds.eyeframe.apiserver.APIObject;
import edu.umass.cs.binds.eyeframe.apiserver.APIRuntimeException;
import edu.umass.cs.binds.eyeframe.apiserver.APIConfiguration;
import edu.umass.cs.binds.eyeframe.apiserver.APIMessageGenerator;

import java.awt.Rectangle;
import java.awt.Point;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import com.fasterxml.jackson.core.JsonGenerator;

public class APICuingOverlay {
    private static APIFunctions<JsonGenerator> APIStaticFunctions;
    private static APIObject APIObjectFunctions;
    private CuingOverlay overlay;

    /**
     * @exclude
     */
    public static void APICuingOverlay() {
        APIObjectFunctions = APIConfiguration.addAPIObject("CuingOverlay");
        APIStaticFunctions = APIConfiguration.addAPIFunctions("CuingOverlay");
        APIStaticFunctions.add("createOverlay",APICuingOverlay::createOverlay);
    }

    private APICuingOverlay(CuingOverlay overlay) {
        this.overlay = overlay;
        APIFunctions<JsonGenerator> apiFunctions =
            APIObjectFunctions.add(overlay.getName());
        apiFunctions.add(
                "setBounds",
                (Integer x,Integer y,Integer w,Integer h) ->
                    setBounds(x,y,w,h));
        apiFunctions.add(
                "setLocation",
                (Integer x,Integer y) -> setLocation(x,y));
        apiFunctions.add(
                "setSize",
                (Integer w,Integer h) -> setSize(w,h));
        apiFunctions.add(
                "setWeightValue",
                (Double w) -> setWeightValue(w));
        apiFunctions.add(
                "setWeightFraction",
                (Double w) -> setWeightFraction(w));
        apiFunctions.add(
                "getLocation",
                () -> getLocation());
        apiFunctions.add(
                "getBounds",
                () -> getBounds());
        apiFunctions.add(
                "getSize",
                () -> getSize());
    }

    /**
     * @exclude
     */
    public synchronized static APICuingOverlay addOverlay(
            CuingOverlay overlay) {
        return new APICuingOverlay(overlay);
    }

    private static APICuingOverlay createOverlay_(int xValue, int yValue,
            int heightValue, int widthValue,
            String nameValue) {
        CuingOverlay overlay = Controller.controller.createOverlay(
                xValue,yValue,
                heightValue,widthValue,
                nameValue);
        return addOverlay(overlay);
    }

    public static JsonGenerator createOverlay(
            Integer xValue, Integer yValue,
            Integer heightValue, Integer widthValue,
            String nameValue,
            Double weightValue) {
        if(Controller.getCuingOverlayGroupManager().overlayNameIsTaken(nameValue)) {
            throw new APIRuntimeException("The Window Name is already taken. Choose a different window name.");
        } else {
            APICuingOverlay overlay = createOverlay_(xValue,yValue,
                    heightValue,widthValue,
                    nameValue);
            if(weightValue != null) {
                overlay.setWeightValue(weightValue);
            }
            return null;
        }
    }

    public synchronized JsonGenerator setBounds(
            Integer x, Integer y,
            Integer width, Integer height) {
        overlay.setBounds(
                x,y,
                width,height);
        return null;
    }

    public synchronized JsonGenerator setSize(Integer width, Integer height) {
        overlay.setSize(width,height);
        return null;
    }

    public synchronized JsonGenerator setLocation(Integer x, Integer y) {
        overlay.setLocation(x,y);
        return null;
    }

    public synchronized JsonGenerator getBounds() {
        try {
            JsonGenerator jsonGenerator = APIMessageGenerator
                .createJsonGenerator();
            jsonGenerator.writeStartObject();
                jsonGenerator.writeNumberField("x",overlay.getPosX());
                jsonGenerator.writeNumberField("y",overlay.getPosY());
                jsonGenerator.writeNumberField("width",overlay.getWidth());
                jsonGenerator.writeNumberField("height",overlay.getHeight());
            jsonGenerator.writeEndObject();
            return jsonGenerator;
        } catch(IOException ex) {
        }
        return null;
    }

    public synchronized JsonGenerator getSize() {
        try {
            JsonGenerator jsonGenerator = APIMessageGenerator
                .createJsonGenerator();
            jsonGenerator.writeStartObject();
                jsonGenerator.writeNumberField("width",overlay.getWidth());
                jsonGenerator.writeNumberField("height",overlay.getHeight());
            jsonGenerator.writeEndObject();
            return jsonGenerator;
        } catch(IOException ex) {
        }
        return null;
    }

    public synchronized JsonGenerator getLocation() {
        try {
            JsonGenerator jsonGenerator = APIMessageGenerator
                .createJsonGenerator();
            jsonGenerator.writeStartObject();
                jsonGenerator.writeNumberField("x",overlay.getPosX());
                jsonGenerator.writeNumberField("y",overlay.getPosY());
            jsonGenerator.writeEndObject();
            return jsonGenerator;
        } catch(IOException ex) {
        }
        return null;
    }

    public synchronized JsonGenerator remove() {
        APIObjectFunctions.remove(overlay.getName());
        overlay.remove();
        return null;
    }

    public synchronized JsonGenerator setWeightFraction(Double weight) {
        overlay.getPieSlice().setWeight(weight);
        return null;
    }

    public synchronized JsonGenerator setWeightValue(Double weight) {
        overlay.getPieSlice().setValue(weight);
        return null;
    }
}
