package edu.umass.cs.binds.eyeframe.apiserver.calls;

import edu.umass.cs.binds.eyeframe.Controller;
import edu.umass.cs.binds.eyeframe.Controller.RunState;
import edu.umass.cs.binds.eyeframe.CuingOverlay;
import edu.umass.cs.binds.eyeframe.MainMenu;
import edu.umass.cs.binds.eyeframe.apiserver.APIFunctions;
import edu.umass.cs.binds.eyeframe.apiserver.APIConfiguration;
import edu.umass.cs.binds.eyeframe.apiserver.APIRuntimeException;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.FileSystems;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.EnumSet;
import java.util.Arrays;
import java.util.Optional;
import com.fasterxml.jackson.core.JsonGenerator;

public class APIController {
    protected static APIFunctions<JsonGenerator> APIStaticFunctions;
    protected static Path loadedConfigFile = null;

    /**
     * @exclude
     */
    public static void APIController() {
        APIStaticFunctions = APIConfiguration.addAPIFunctions("Controller");
        APIStaticFunctions
            .add("switchToController",APIController::switchToController)
            .add("loadConfigFile",APIController::loadConfigFile)
            .add("start",APIController::start)
            .add("stop",APIController::stop)
            .add("saveToConfigFile",APIController::saveToConfigFile);
    }

    private APIController() {}

    public synchronized static JsonGenerator switchToController() {
        MainMenu.mainMenu.switchToController(null);
        return null;
    }

    public synchronized static JsonGenerator loadConfigFile(
            String configFilePath) {
        loadConfigFile_(FileSystems.getDefault().getPath(configFilePath));
        return null;
    }

    protected synchronized static JsonGenerator loadConfigFile_(
            Path configFile) {
        loadedConfigFile = configFile;
        Controller.controller.load(configFile.toFile());
        for (CuingOverlay overlay : Controller.getListOfOverlays()) {
            APICuingOverlay.addOverlay(overlay);
        }
        return null;
    }

    public synchronized static JsonGenerator start(
            String[] runStates, String useEyeTracker) {
        EnumSet<Controller.RunState> runState =
            (EnumSet<Controller.RunState>) Arrays.stream(runStates)
            .reduce(EnumSet.noneOf(Controller.RunState.class),
                    (e,s) -> {
                        e.add(Controller.RunState.valueOf(s));
                        return e;
                    },
                    (e1,e2) -> {
                        e1.addAll(e2);
                        return e1;
                    });
        if(runState.isEmpty()) {
                throw new APIRuntimeException("You must select Recording, Alert, or both");
        } else {
            Controller.controller.start(runState,
                    Boolean.parseBoolean(useEyeTracker));
        }
        return null;
    }

    public synchronized static JsonGenerator stop() {
        Controller.controller.stop();
        return null;
    }

    public synchronized static JsonGenerator saveToConfigFile(String path) {
        if(path == null) {
            Controller.controller.save(loadedConfigFile.toString());
        } else {
            Controller.controller.save(path);
        }
        return null;
    }
}
