package edu.umass.cs.binds.eyeframe.apiserver.calls;

import edu.umass.cs.binds.eyeframe.MainMenu;
import edu.umass.cs.binds.eyeframe.SettingsPanel;
import edu.umass.cs.binds.eyeframe.apiserver.APIFunctions;
import edu.umass.cs.binds.eyeframe.apiserver.APIConfiguration;

import com.fasterxml.jackson.core.JsonGenerator;
import java.awt.Color;

public class APISettingsPanel {
    private static APIFunctions<JsonGenerator> APIStaticFunctions;
    private static SettingsPanel settingsPanel;
    
    /**
     * @exclude
     */
    public static void APISettingsPanel() {
        APIStaticFunctions = APIConfiguration.addAPIFunctions("SettingsPanel");
        APIStaticFunctions
            .add("switchToSettingsPanel",
                    APISettingsPanel::switchToSettingsPanel)
            .add("changeOverlayTypeColor",
                    APISettingsPanel::changeOverlayTypeColor)
            .add("enableOverlayBorderMode",
                    APISettingsPanel::enableOverlayBorderMode)
            .add("setOverlayBorderThickness",
                    APISettingsPanel::setOverlayBorderThickness);
    }

    private APISettingsPanel() {}

    public synchronized static JsonGenerator switchToSettingsPanel() {
        MainMenu.mainMenu.switchToSettingsPanel();
        settingsPanel = SettingsPanel.settingsPanel;
        return null;
    }

    public synchronized static JsonGenerator changeOverlayTypeColor(
            Integer rgba, Boolean hasalpha,
            String overlayType) {
        //Creates an sRGB color with the specified combined RGBA value consisting of the alpha component in bits 24-31, the red component in bits 16-23, the green component in bits 8-15, and the blue component in bits 0-7.
        Color color = new Color(rgba,hasalpha);
        if(overlayType == "primary") {
            settingsPanel.changeColor(color,settingsPanel.getCtOne());
        } else if(overlayType == "secondary") {
            settingsPanel.changeColor(color,settingsPanel.getCtTwo());
        } else {
            settingsPanel.changeColor(color,settingsPanel.getCtRegular());
        }
        return null;
    }

    public synchronized static JsonGenerator enableOverlayBorderMode(Boolean b) {
        settingsPanel.enableOverlayBorderMode(b);
        return null;
    }

    public synchronized static JsonGenerator setOverlayBorderThickness(
            Integer thickness) {
        settingsPanel.setOverlayBorderThickness(thickness);
        return null;
    }
}
