package edu.umass.cs.binds.eyeframe.apiserver;

import java.util.Map;
import java.util.HashMap;
import com.fasterxml.jackson.core.JsonGenerator;

public class APIObject {
    protected Map<String,APIFunctions<JsonGenerator>> objectFunctions;

    APIObject() {
        objectFunctions = new HashMap<>();
    }

    public APIFunctions<JsonGenerator> add(String objectName) {
        APIFunctions<JsonGenerator> apiFunctions = new APIFunctions<JsonGenerator>();
        objectFunctions.put(objectName,apiFunctions);
        return apiFunctions;
    }

    public APIObject add(String objectName, APIFunctions<JsonGenerator> apiFunctions) {
        objectFunctions.put(objectName,apiFunctions);
        return this;
    }

    public APIFunctions<JsonGenerator> get(String objectName) {
        return objectFunctions.get(objectName);
    }

    public boolean containsKey(String objectName) {
        return objectFunctions.containsKey(objectName);
    }

    public APIObject remove(String objectName) {
        objectFunctions.remove(objectName);
        return this;
    }
}
