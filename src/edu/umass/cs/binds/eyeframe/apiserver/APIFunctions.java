package edu.umass.cs.binds.eyeframe.apiserver;

import java.util.Map;
import java.util.HashMap;

public class APIFunctions<R> {
    protected Map<String,APIFunction<R>> functions;

    APIFunctions() {
        functions = new HashMap<>();
    }

    public APIFunction<R> get(String functionName) {
        return functions.get(functionName);
    }

    public APIFunctions<R> add(String functionName,
            APIFunction<R> function) {
        functions.put(functionName,function);
        return this;
    }

    public APIFunctions<R> add(String functionName,
            APIFunction0<R> function) {
        add(functionName,(APIFunction<R>) function);
        return this;
    }

    public <T0> APIFunctions<R> add(String functionName,
            APIFunction1<T0,R> function) {
        add(functionName,(APIFunction<R>) function);
        return this;
    }

    public <T0,T1> APIFunctions<R> add(String functionName,
            APIFunction2<T0,T1,R> function) {
        add(functionName,(APIFunction<R>) function);
        return this;
    }

    public <T0,T1,T2> APIFunctions<R> add(String functionName,
            APIFunction3<T0,T1,T2,R> function) {
        add(functionName,(APIFunction<R>) function);
        return this;
    }

    public <T0,T1,T2,T3> APIFunctions<R> add(String functionName,
            APIFunction4<T0,T1,T2,T3,R> function) {
        add(functionName,(APIFunction<R>) function);
        return this;
    }

    public <T0,T1,T2,T3,T4> APIFunctions<R> add(String functionName,
            APIFunction5<T0,T1,T2,T3,T4,R> function) {
        add(functionName,(APIFunction<R>) function);
        return this;
    }

    public <T0,T1,T2,T3,T4,T5>
        APIFunctions<R> add(String functionName,
            APIFunction6<T0,T1,T2,T3,T4,T5,R> function) {
        add(functionName,(APIFunction<R>) function);
        return this;
    }
}


