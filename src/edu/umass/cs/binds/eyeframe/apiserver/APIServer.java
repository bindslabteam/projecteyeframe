package edu.umass.cs.binds.eyeframe.apiserver;

import edu.umass.cs.binds.eyeframe.apiserver.calls.APISettingsPanel;
import edu.umass.cs.binds.eyeframe.apiserver.calls.APIController;
import edu.umass.cs.binds.eyeframe.apiserver.calls.APICuingOverlay;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Scanner;

public class APIServer implements Runnable {
    private ServerSocketChannel serverSocket;
    private String ipAddress;
    private int port;
    private volatile Thread currentThread;
    private APIServerProtocol protocol;

    public APIServer(String ipAddress, int port) throws IOException {
        configureConnection(ipAddress,port);
        protocol = new APIServerProtocol();
        APISettingsPanel.APISettingsPanel();
        APIController.APIController();
        APICuingOverlay.APICuingOverlay();
    }

    public void start() {
        currentThread = new Thread(this);
        currentThread.start();
        protocol.start();
    }

    public void stop() {
        currentThread = null;
        protocol.stop();
    }

    public boolean isStopped() {
        if(this.serverSocket == null) {
            return true;
        }
        return false;
    }

    public void configureConnection(String ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void run() {
        try (ServerSocketChannel serverSocket = ServerSocketChannel.open()) {
            this.serverSocket = serverSocket;
            //serverSocket.configureBlocking(false);
            serverSocket.bind(new InetSocketAddress(ipAddress,port));
            Thread thisThread = Thread.currentThread();
            while(currentThread == thisThread) {
                try {
                    SocketChannel socketChannel = serverSocket.accept();
                    protocol.addSocketChannel(socketChannel);
                } catch(IOException ex){
                    ex.printStackTrace();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.serverSocket = null;
    }

    protected void finalize() {
        stop();
    }
}
