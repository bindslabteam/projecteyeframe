package edu.umass.cs.binds.eyeframe.apiserver;

@FunctionalInterface
public interface APIFunction0<R> extends APIFunction<R> {

    @SuppressWarnings("unchecked")
    default R apply(Object... args) {
        return method();
    }

    R method();
}

