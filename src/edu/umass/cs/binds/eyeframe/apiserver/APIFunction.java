package edu.umass.cs.binds.eyeframe.apiserver;

interface APIFunction<R> {
    R apply(Object... args);
}
