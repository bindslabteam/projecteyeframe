package edu.umass.cs.binds.eyeframe.apiserver;

import java.util.Map;
import java.util.HashMap;
import com.fasterxml.jackson.core.JsonGenerator;

public final class APIConfiguration {
    protected static Map<String,APIFunctions<JsonGenerator>> staticFunctions =
        new HashMap<>();
    protected static Map<String,APIObject> objectFunctions =
        new HashMap<>();

    private APIConfiguration() {}

    public static APIObject addAPIObject(String className) {
        APIObject apiObject = new APIObject();
        objectFunctions.put(className,apiObject);
        return apiObject;
    }

    public static void addAPIObject(String className, APIObject apiObject) {
        objectFunctions.put(className,apiObject);
    }

    public static APIFunctions<JsonGenerator> addAPIFunctions(String className) {
        APIFunctions<JsonGenerator> apiFunctions = new APIFunctions<JsonGenerator>();
        staticFunctions.put(className,apiFunctions);
        return apiFunctions;
    }

    public static void addAPIFunctions(String className,
            APIFunctions<JsonGenerator> apiFunctions) {
        staticFunctions.put(className,apiFunctions);
    }

    public static boolean hasAPIObject(String className) {
        return objectFunctions.containsKey(className);
    }

    public static boolean hasAPIFunctions(String className,
            String objectName) {
        return objectFunctions.get(className)
            .containsKey(objectName);
    }

    public static boolean hasAPIFunctions(String className) {
        return staticFunctions.containsKey(className);
    }

    public static APIObject getAPIObject(String className) {
        return objectFunctions.get(className);
    }

    public static APIFunctions<JsonGenerator> getAPIFunctions(String className,
            String objectName) {
        return objectFunctions.get(className)
            .get(objectName);
    }

    public static APIFunctions<JsonGenerator> getAPIFunctions(String className) {
        return staticFunctions.get(className);
    }
}
