package edu.umass.cs.binds.eyeframe.apiserver;

@FunctionalInterface
public interface APIFunction6<T0,T1,T2,T3,T4,T5,R> extends APIFunction<R> {

    @SuppressWarnings("unchecked")
    default R apply(Object... args) {
        return method(
                (T0) args[0],
                (T1) args[1],
                (T2) args[2],
                (T3) args[3],
                (T4) args[4],
                (T5) args[5]);
    }

    R method(
            T0 arg0,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5);
}

