package edu.umass.cs.binds.eyeframe.apiserver;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
// maybe use the fulle jackson lib to utilize the node factory instead of the
// generator?
//import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.io.IOException;

public class APIMessageGenerator {
    private static JsonFactory jsonFactory = new JsonFactory();

    private APIMessageGenerator() {}

    public static JsonGenerator createJsonGenerator() throws IOException {
        return jsonFactory.createGenerator(
                new StringWriter());
    }
}
