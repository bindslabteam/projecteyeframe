package edu.umass.cs.binds.eyeframe.apiserver;

import java.lang.RuntimeException;

public class APIRuntimeException extends RuntimeException {
    public APIRuntimeException(String message) {
        super(message);
    }
}
