package edu.umass.cs.binds.eyeframe.apiserver;

import java.nio.channels.Selector;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.nio.ByteBuffer;

class APIServerProtocol implements Runnable {
    private Selector selector;
    private Set<APIMessageParser> activeParsers;
    protected volatile Thread currentThread;
    private ByteBuffer byteBuffer;

    APIServerProtocol() throws IOException {
        this.selector = Selector.open();
        this.activeParsers = new HashSet<>();
        byteBuffer = ByteBuffer.allocate(1024);
    }

    public void start() {
        currentThread = new Thread(this);
        currentThread.start();
    }

    public void stop() {
        currentThread = null;
    }

    public void run() {
        Thread thisThread = Thread.currentThread();
        try {
            while(currentThread == thisThread) {
                if(selector.selectNow() > 0) {
                    Set<SelectionKey> selectedKeys = selector.selectedKeys();
                    for(SelectionKey key : selectedKeys) {
                        byteBuffer.clear();
                        int n = ((SocketChannel) key.channel())
                            .read(byteBuffer);
                        APIMessageParser parser = 
                            (APIMessageParser) key.attachment();
                        if(n == -1) {
                            parser.close();
                            activeParsers.remove(parser);
                        } else if(n > 0) {
                            parser.processMessage(n,byteBuffer);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            stop();
            //TODO: show error to user, maybe pause the program
        }
        for(APIMessageParser messageParser: activeParsers) {
            try {
                messageParser.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                //TODO: decide if an error needs to be displayed for this
            }
        }
        activeParsers.clear();
    }

    public void addSocketChannel(SocketChannel socketChannel)
            throws IOException {
        try {
            socketChannel.configureBlocking(false);
            APIMessageParser messageParser = new APIMessageParser(socketChannel);
            activeParsers.add(messageParser);
            SelectionKey selectionKey = socketChannel.register(selector,
                    SelectionKey.OP_READ, messageParser);
        } catch (ClosedChannelException ex) {
            ex.printStackTrace();
            //TODO: show error?
        }
    }

    protected void finalize() {
        stop();
    }
}
