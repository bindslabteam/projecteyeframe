package edu.umass.cs.binds.eyeframe.apiserver;

@FunctionalInterface
public interface APIFunction1<T0,R> extends APIFunction<R> {

    @SuppressWarnings("unchecked")
    default R apply(Object... args) {
        return method((T0) args[0]);
    }

    R method(T0 arg0);
}

