package edu.umass.cs.binds.eyeframe;

import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.List;

class CuingOverlayGroupSequenceQueueView {
    private List<CuingOverlay> sequence;
    private ListIterator<CuingOverlay> iterator;
    private CuingOverlay currentOverlay;
    private CuingOverlay nextOverlay;
    private int size;

    CuingOverlayGroupSequenceQueueView(List<CuingOverlay> sequence) {
        this.sequence = sequence;
        reset();
    }

    public void reset() {
        iterator = sequence.listIterator();
        size = sequence.size();
        currentOverlay = null;
        nextOverlay = null;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public CuingOverlay first() {
        return peek();
    }

    public CuingOverlay second() {
        peek();
        if(nextOverlay == null) {
            if(iterator.hasNext()) {
                nextOverlay = iterator.next();
            } else {
                return null;
            }
        }
        return nextOverlay;
    }

    private CuingOverlay peek() {
        if(currentOverlay == null) {
            if(iterator.hasNext()) {
                currentOverlay = iterator.next();
            } else {
                return null;
            }
        }
        return currentOverlay;
    }

    private CuingOverlay poll() {
        CuingOverlay currentOverlay = this.currentOverlay;
        this.currentOverlay = this.nextOverlay;
        this.nextOverlay = null;
        if(currentOverlay == null) {
            if(iterator.hasNext()) {
                currentOverlay = iterator.next();
                size--;
            } else {
                return null;
            }
        } else {
            size--;
        }
        return currentOverlay;
    }

    public CuingOverlay remove() {
        CuingOverlay overlay = poll();
        if(overlay == null) {
            throw new NoSuchElementException();
        }
        return overlay;
    }
}
