package edu.umass.cs.binds.eyeframe;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

class CuingOverlayGroup extends CuingOverlay.Group {
    public static enum RunState {
        NO_GROUP,
        RIGID_TRIGGER_SEQUENCE_OUTOFORDER,
        RIGID_TRIGGER_SEQUENCE_INORDER,
        RIGID_TRIGGER_SEQUENCE_STRICT_OUTOFORDER,
        RIGID_TRIGGER_SEQUENCE_STRICT_INORDER,
    }
    public abstract static class Manager {
        abstract protected void addGroup(CuingOverlayGroup group);
        abstract protected void addOverlay(CuingOverlay overlay);
        abstract protected void removeOverlay(CuingOverlay overlay);
        abstract protected void removeGroup(CuingOverlayGroup group);
    }

    protected LinkedList<CuingOverlay> overlays;
    protected RunState type;
    protected String name;
    protected CuingOverlayGroupManager manager;

    CuingOverlayGroup(String name,
            CuingOverlayGroupManager manager) {
        this(name, RunState.NO_GROUP, manager, new LinkedList<CuingOverlay>());
    }

    protected CuingOverlayGroup(String name,
            RunState type,
            CuingOverlayGroupManager manager) {
        this(name, type, manager, new LinkedList<CuingOverlay>());
    }

    protected CuingOverlayGroup(String name, RunState type,
            CuingOverlayGroupManager manager,
            LinkedList<CuingOverlay> overlays) {
        this.name = name;
        this.manager = manager;
        this.manager.addGroup(this);
        this.overlays = overlays;
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected void addOverlay(CuingOverlay overlay) {
        overlays.add(overlay);
        manager.addOverlay(overlay);
    }

    protected void addOverlay(CuingOverlay overlay, int index) {
        overlays.add(index,overlay);
        manager.addOverlay(overlay);
    }

    protected void removeOverlay(CuingOverlay overlay) {
        int index = overlay.getGroupPosition();
        if(index > -1) {
            overlays.remove(index);
        }
        manager.removeOverlay(overlay);
    }

    protected void moveOverlay(int fromIndex, int toIndex) {
        if(fromIndex != toIndex) {
            CuingOverlay overlay = this.overlays.remove(fromIndex);
            if(toIndex >= this.overlays.size()) {
                this.overlays.add(overlay);
            } else {
                this.overlays.add(toIndex,overlay);
            }
        }
    }

    protected int getOverlayPosition(CuingOverlay overlay) {
        return overlays.indexOf(overlay);
    }

    public RunState getType() {
        return type;
    }

    public int size() {
        return overlays.size();
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return getName();
    }

    public CuingOverlayGroupManager getManager() {
        return manager;
    }

    public List<CuingOverlay> getOverlays() {
        return overlays;
    }

    public void setOverlays(LinkedList<CuingOverlay> overlays) {
        this.overlays = overlays;
    }

    public void disableAlerting() {
        for(CuingOverlay overlay: overlays) {
            overlay.setAlertingForNextEyeFrameRun(false);
            overlay.hide();
        }
    }

    public void setAsDefaultGroup() {
        manager.setDefaultGroup(this);
    }

    public void setup() {}

    public void focusedOnOverlay(CuingOverlay overlay,
            CuingOverlay lastOverlay, Stack stack) {}

    public void remove() {
        manager.removeGroup(this);
        remove_();
    }

    protected void remove_() {
        for(CuingOverlay overlay : overlays) {
            overlay.remove_();
        }
        this.overlays = null;
    }

    public Map<String,Object> toMap() {
        HashMap<String,Object> variables = new HashMap<>(4);
        variables.put("type",type.toString());
        variables.put("name",name);
        ArrayList<Map<String,Object>> overlaysList =
            new ArrayList<>(overlays.size());
        for(CuingOverlay overlay : overlays) {
            overlaysList.add(overlay.toMap());
        }
        variables.put("overlays",overlaysList);
        return variables;
    }

    public static CuingOverlayGroup toObject(
            Map<String,Object> map,
            CuingOverlayGroupManager manager,
            PieGraph pieGraph,
            OverlayPieGraphLegend pieGraphLegend,
            String defaultGroupName) throws Exception {
        RunState type = (RunState) RunState.valueOf((String) map.get("type"));

        String name = (String) map.get("name");
        int triggerSequencePos = map.containsKey("triggerSequencePos") ?
            (int) map.get("triggerSequencePos") : -1;
        CuingOverlayGroup group = createCuingOverlayGroup(name,type,manager,
                triggerSequencePos);
        if(group == null) {
            throw new Exception(">>>>>");
            //TODO: throw group not implemented
        }

        if(name.compareTo(defaultGroupName) == 0) {
            group.setAsDefaultGroup();
            pieGraphLegend.setCuingOverlayGroup(group);
        }

        List<Map<String,Object>> overlaysMaps =
            (List<Map<String,Object>>) map.get("overlays");
        for(Map<String,Object> overlayMap: overlaysMaps) {
            CuingOverlay overlay = CuingOverlay.toObject(
                    overlayMap,
                    group,
                    pieGraph,
                    pieGraphLegend);
        }

        Controller.controller.addGroupToUI(group);
        return group;
    }

    public static CuingOverlayGroup createCuingOverlayGroup(
            String name,
            CuingOverlayGroup.RunState type,
            CuingOverlayGroupManager manager,
            int triggerSequencePos) {
        switch(type) {
        case NO_GROUP:
            return new CuingOverlayGroup(name,manager);
        case RIGID_TRIGGER_SEQUENCE_OUTOFORDER:
            return new CuingOverlayGroupRigidTriggerSequenceOutOfOrder(
                    name,manager,triggerSequencePos);
        case RIGID_TRIGGER_SEQUENCE_INORDER:
            return new CuingOverlayGroupRigidTriggerSequenceInOrder(
                    name,manager,triggerSequencePos);
        case RIGID_TRIGGER_SEQUENCE_STRICT_OUTOFORDER:
            return new CuingOverlayGroupRigidTriggerSequenceStrictOutOfOrder(
                    name,manager,triggerSequencePos);
        case RIGID_TRIGGER_SEQUENCE_STRICT_INORDER:
            return new CuingOverlayGroupRigidTriggerSequenceStrictInOrder(
                    name,manager,triggerSequencePos);
        default:
            //TODO: throw group not implemented
            return null;
        }
    }
}
