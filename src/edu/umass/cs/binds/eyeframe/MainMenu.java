package edu.umass.cs.binds.eyeframe;

import edu.umass.cs.binds.eyeframe.apiserver.APIServer;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.JWindow;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.FontUIResource;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FilenameFilter;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JScrollPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import java.io.IOException;
import java.util.Map.Entry;
import net.miginfocom.swing.MigLayout;

public class MainMenu {

    private JFrame frmEyeframeApplication;
    public static MainMenu mainMenu;
    private final ButtonGroup buttonGroup = new ButtonGroup();
    protected JFileChooser fileChooser;
    protected File selectedFolder;
    protected JRadioButton rdbtnEyeTracker;
    protected JButton btnSelect;
    protected JLabel lblSelectAFolder;
    protected APIServer apiServer;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    setupUI();
                    mainMenu = new MainMenu();
                    mainMenu.frmEyeframeApplication.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public MainMenu() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmEyeframeApplication = new JFrame();
        frmEyeframeApplication.getContentPane().setFont(UIManager.getFont("Button.font"));
        frmEyeframeApplication.setTitle("EyeFrame Application");
        frmEyeframeApplication.setBounds(100, 100, 663, 456);
        frmEyeframeApplication.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MigLayout migLayout = new MigLayout("", "[][5mm][grow][]", "[][][][3mm][][][grow][]");
        frmEyeframeApplication.getContentPane().setLayout(migLayout);

        JTextPane txtpnEyeframe = new JTextPane();
        txtpnEyeframe.setFont(new Font("Malgun Gothic", Font.BOLD | Font.ITALIC, 25));
        txtpnEyeframe.setBackground(UIManager.getColor("Button.background"));
        txtpnEyeframe.setText("EyeFrame");
        frmEyeframeApplication.getContentPane().add(txtpnEyeframe, "cell 2 0,alignx center,aligny center");

        JButton btnSettings = new JButton("Settings");
        btnSettings.setFont(UIManager.getFont("Button.font"));
        btnSettings.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        switchToSettingsPanel();
                    }
                });
            }
        });
        frmEyeframeApplication.getContentPane().add(btnSettings, "cell 0 1,growx,aligny top");

        JButton btnNew = new JButton("New");
        btnNew.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EventQueue.invokeLater(new Runnable() {
                    public void run() {	
                        switchToController(null);
                    }
                });
            }
        });
        btnNew.setFont(UIManager.getFont("Button.font"));
        frmEyeframeApplication.getContentPane().add(btnNew, "cell 3 1,growx,aligny top");

        JScrollPane scrollPane = new JScrollPane();
        frmEyeframeApplication.getContentPane().add(scrollPane, "cell 0 2 3 5,grow");

        JList<File> list = new JList<File>();
        scrollPane.setViewportView(list);
        list.setFont(UIManager.getFont("List.font"));
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JButton btnOpen = new JButton("Open");
        btnOpen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        if (selectedFolder == null) {
                            JOptionPane.showMessageDialog(null,
                                    "Please Choose a Folder Directory",
                                    "EyeFrame Error", JOptionPane.INFORMATION_MESSAGE);
                            btnSelect.requestFocusInWindow();
                        } else if (list.isSelectionEmpty()) {
                            JOptionPane.showMessageDialog(null,
                                    "Please select a file from the list",
                                    "EyeFrame Error", JOptionPane.INFORMATION_MESSAGE);
                            list.requestFocusInWindow();
                        } else {
                            switchToController(selectedFolder.toPath()
                                    .resolve(list.getSelectedValue().toPath()).toFile());
                        }
                    }
                });
            }
        });
        btnOpen.setFont(UIManager.getFont("Button.font"));
        frmEyeframeApplication.getContentPane().add(btnOpen, "cell 3 2,growx,aligny top");

        JRadioButton rdbtnMouse = new JRadioButton("Mouse");
        rdbtnMouse.setFont(UIManager.getFont("RadioButton.font"));
        rdbtnMouse.setSelected(true);
        buttonGroup.add(rdbtnMouse);
        frmEyeframeApplication.getContentPane().add(rdbtnMouse, "cell 3 4,growx,aligny top");

        btnSelect = new JButton("Change Folder");
        btnSelect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        JWindow wndFileChooser = new JWindow();
                        wndFileChooser.setAlwaysOnTop(true);
                        File selectedFile = null;
                        int returnVal = fileChooser.showOpenDialog(wndFileChooser);
                        if (returnVal == JFileChooser.APPROVE_OPTION) {
                            selectedFolder = fileChooser.getSelectedFile();
                            if(selectedFolder.getName().endsWith(".eyeframe")) {
                                selectedFile = selectedFolder;
                                selectedFolder = selectedFolder.toPath().getParent().toFile();
                            }
                            lblSelectAFolder.setText(selectedFolder.getAbsolutePath());
                            File[] configFiles = selectedFolder.listFiles(new FilenameFilter() {
                                public boolean accept(File folder, String filename) {
                                    return filename.endsWith(".eyeframe");
                                }
                            });
                            list.setListData(configFiles);
                            if(selectedFile != null) {
                                list.setSelectedValue(selectedFile, true);
                            }
                        }
                    }
                });
            }
        });

        rdbtnEyeTracker = new JRadioButton("Eye Tracker");
        rdbtnEyeTracker.setFont(UIManager.getFont("RadioButton.font"));
        buttonGroup.add(rdbtnEyeTracker);
        frmEyeframeApplication.getContentPane().add(rdbtnEyeTracker, "cell 3 5,growx,aligny top");

        btnSelect.setFont(UIManager.getFont("Button.font"));
        frmEyeframeApplication.getContentPane().add(btnSelect, "cell 0 7,alignx left,aligny top");

        lblSelectAFolder = new JLabel("Select Folder Directory");
        lblSelectAFolder.setLabelFor(btnSelect);
        lblSelectAFolder.setFont(UIManager.getFont("Label.font"));
        frmEyeframeApplication.getContentPane().add(lblSelectAFolder, "cell 2 7,growx,aligny top");

        fileChooser = new JFileChooser(".");
        fileChooser.setFileFilter(new FolderFilter());
        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        frmEyeframeApplication.getContentPane().setMinimumSize(migLayout.preferredLayoutSize(frmEyeframeApplication.getContentPane()));

        startServer();
    }

    public void restartServer() {
        if(apiServer != null
                && (apiServer.getPort() != SettingsPanel.port
                    || apiServer.getIpAddress() != SettingsPanel.ipAddress)) {
            apiServer.stop();
            startServer();
        }
    }

    public void startServer() {
        try {
            apiServer = new APIServer(SettingsPanel.ipAddress,
                    SettingsPanel.port);
            apiServer.start();
        } catch(IOException ex) {
            ex.printStackTrace();
            //TODO: display error
        }
    }

    public void switchToController(File configFile) {
        if(SettingsPanel.settingsPanel != null) {
            SettingsPanel.settingsPanel.setVisible(false);
        }
        Controller controller = Controller.controller;
        if(controller == null) {
            controller = new Controller(rdbtnEyeTracker.isSelected());
            controller.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    frmEyeframeApplication.setVisible(true);
                    frmEyeframeApplication.toFront();
                    Controller.controller = null;
                }
            });
        }
        if(configFile != null) {
            controller.load(configFile);
        }
        frmEyeframeApplication.setVisible(false);
        controller.setVisible(true);
    }

    public void switchToSettingsPanel() {
        if(Controller.controller != null) {
            Controller.controller.setVisible(false);
        }
        SettingsPanel frmSettings = new SettingsPanel();
        frmSettings.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                frmEyeframeApplication.setVisible(true);
                frmEyeframeApplication.toFront();
            }
        });
        frmEyeframeApplication.setVisible(false);
        frmSettings.setVisible(true);
    }

    private static class FolderFilter extends
        javax.swing.filechooser.FileFilter {
            @Override
            public boolean accept(File file) {
                if(file.isFile()) {
                    return file.getName().endsWith(".eyeframe");
                } else {
                    return file.isDirectory();
                }
            }

            @Override
            public String getDescription() {
                return "We only take directories";
            }
        }

    public static void setupUI() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        }
        UIManager.put("Label.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        UIManager.put("TextField.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        UIManager.put("Spinner.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        UIManager.put("FormattedTextField.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        UIManager.put("Button.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("ToggleButton.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        UIManager.put("RadioButton.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        UIManager.put("CheckBox.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("ColorChooser.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        UIManager.put("ComboBox.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("Label.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        UIManager.put("List.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("MenuBar.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("MenuItem.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("RadioButtonMenuItem.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("CheckBoxMenuItem.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("Menu.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("PopupMenu.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("OptionPane.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("Panel.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("ProgressBar.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("ScrollPane.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("Viewport.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("TabbedPane.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        UIManager.put("Table.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        UIManager.put("TableHeader.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("TextField.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("PasswordField.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("TextArea.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("TextPane.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("EditorPane.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("TitledBorder.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("ToolBar.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("ToolTip.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //    UIManager.put("Tree.font", new FontUIResource("Tahoma", Font.PLAIN, 20));
        //      for (Entry<Object, Object> entry : javax.swing.UIManager.getDefaults().entrySet()) {
        //          String key = (String) entry.getKey().toString();
        //          if(key.matches("^F.*")) {
        //              Object value = javax.swing.UIManager.get(key);
        //              if (value != null && value instanceof javax.swing.plaf.FontUIResource) {
        //                  System.out.println(key);
        //                  javax.swing.plaf.FontUIResource fr=(javax.swing.plaf.FontUIResource)value;
        //                  javax.swing.plaf.FontUIResource f = new javax.swing.plaf.FontUIResource(fr.getFamily(), fr.getStyle(), 20);
        //                  javax.swing.UIManager.put(key, f);
        //              }
        //          }
        //      }
    }
}
