package edu.umass.cs.binds.eyeframe;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.LinkedHashSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JComponent;
import javax.swing.JWindow;

import javax.swing.border.LineBorder;

import javafx.scene.paint.Color;

//add a new class that adds a JWindow with time
public class Stack {

    private Map<CuingOverlay, Double> weightMap;
    private LinkedList<CuingOverlayGroupSequenceQueueView> sequences;
    private LinkedHashSet<CuingOverlayGroup> sequencesGroups;
    private CuingOverlayGroup currentSequenceGroup;
    private CuingOverlayGroupSequenceQueueView currentSequence;
    private CuingOverlay mostNeglected;
    private CuingOverlay secondMostNeglected;
    private IRecorder recorder;
    private CuingOverlay[] stack;

    Stack(Set<CuingOverlay> listOfWindows, IRecorder recorder) {
        this.recorder = recorder;
        weightMap = new HashMap<CuingOverlay, Double>();
        for (CuingOverlay ct : listOfWindows) {
            weightMap.put(ct, ct.getWeightValue());
        }
        sequences = new LinkedList<>();
        sequencesGroups = new LinkedHashSet<>();
        currentSequence = null;
        currentSequenceGroup = null;
        stack = new CuingOverlay[2];
    }

    public void setup() {
        computeStack();
        mostNeglected = stack[0];
        secondMostNeglected = stack[1];
        for(CuingOverlay ct : weightMap.keySet()) {
            ct.eyeFrameRunStarted();
            if(ct == mostNeglected) {
                ct.setColor(SettingsPanel.mostNeglected);
            } else if(ct == secondMostNeglected) {
                ct.setColor(SettingsPanel.secondNeglected);
            } else {
                ct.setColor(SettingsPanel.regColor);
            }
        }
        currentSequence = null;
        currentSequenceGroup = null;
    }

    public void cleanup() {
        currentSequence = null;
        currentSequenceGroup = null;
        for(CuingOverlay ct : weightMap.keySet()) {
            ct.eyeFrameRunStopped();
        }
        recorder.saveEvents();
    }

    public void addGroupSequence(CuingOverlayGroup group,
            CuingOverlayGroupSequenceQueueView sequence) {
        sequencesGroups.add(group);
        sequences.add(sequence);
        changeColors();
    }

    public boolean isASequenceGroup(CuingOverlayGroup group) {
        return (sequencesGroups.contains(group)
                || currentSequenceGroup == group);
    }

    public CuingOverlayGroup getCurrentSequenceGroup() {
        return currentSequenceGroup;
    }

    public CuingOverlay getMostNeglected() {
        return mostNeglected;
    }

    public CuingOverlay getSecondMostNeglected() {
        return secondMostNeglected;
    }

    protected void computeSequenceStack() {
        CuingOverlay largestOverlay = stack[0];
        CuingOverlay secondLargestOverlay = stack[1];

        if(currentSequence != null && currentSequence.size() > 0) {
            if(currentSequence.size() >= 2) {
                largestOverlay = currentSequence.first();
                secondLargestOverlay = currentSequence.second();
            } else if(sequences.size() > 0) {
                largestOverlay = currentSequence.first();
                secondLargestOverlay = sequences.peek().first();
            } else if(largestOverlay != currentSequence.first()) {
                secondLargestOverlay = largestOverlay;
                largestOverlay = currentSequence.first();
            }

            stack[0] = largestOverlay;
            stack[1] = secondLargestOverlay;
        } else {
            if(currentSequence != null) {
                currentSequenceGroup.setup();
            }
            if(sequences.size() > 0) {
                currentSequence = sequences.pop();
                currentSequenceGroup = sequencesGroups.iterator().next();
                sequencesGroups.remove(currentSequenceGroup);
                computeSequenceStack();
            } else {
                currentSequence = null;
                currentSequenceGroup = null;
            }
        }
    }

    protected void computeStack() {
        CuingOverlay largestOverlay = null;
        CuingOverlay secondLargestOverlay = null;
        Double largestValue = -1.0;
        Double secondLargestValue = -1.0;
        Double compareValue = null;

        for (Entry<CuingOverlay, Double> entry : weightMap.entrySet()) {
            compareValue = entry.getValue();
            if(compareValue > largestValue) {
                secondLargestOverlay = largestOverlay;
                secondLargestValue = largestValue;
                largestOverlay = entry.getKey();
                largestValue = compareValue;
            } else if(compareValue > secondLargestValue) {
                secondLargestOverlay = entry.getKey();
                secondLargestValue = compareValue;
            }
        }
        stack[0] = largestOverlay;
        stack[1] = secondLargestOverlay;
        computeSequenceStack();
    }

    public void changeColors() {
        computeStack();
        if (weightMap.size() == 1) {
            CuingOverlay first = stack[0];
            if(first != mostNeglected) {
                mostNeglected = first;
                first.setColor(SettingsPanel.mostNeglected);
            }
        } else if (weightMap.size() == 2) {
            CuingOverlay first = stack[0];
            CuingOverlay last = stack[1];
            if(first != mostNeglected) {
                first.setColor(SettingsPanel.mostNeglected);
                last.setColor(SettingsPanel.regColor);
                mostNeglected = first;
            }
        } else {
            CuingOverlay first = stack[0];
            CuingOverlay second = stack[1];
            if(first == mostNeglected) {
                if(second != secondMostNeglected) {
                    second.setColor(SettingsPanel.secondNeglected);
                    secondMostNeglected
                        .setColor(SettingsPanel.regColor);
                    secondMostNeglected = second;
                }
            } else {
                first.setColor(SettingsPanel.mostNeglected);
                if(second == secondMostNeglected) {
                    mostNeglected
                        .setColor(SettingsPanel.regColor);
                } else {
                    second.setColor(SettingsPanel.secondNeglected);
                    if(first != secondMostNeglected) {
                        secondMostNeglected
                            .setColor(SettingsPanel.regColor);
                    }
                    if(second != mostNeglected) {
                        mostNeglected
                            .setColor(SettingsPanel.regColor);
                    }
                    secondMostNeglected = second;
                }
                mostNeglected = first;
            }
        }
    }

    public void resetOverlayWeight(CuingOverlay cuingOverlay) {
        weightMap.replace(cuingOverlay, 0.0);
        //System.out.println(cuingOverlay.getName());
        recorder.addEvent("looked at: ", cuingOverlay.getName(), false);
        //changeColors();
    }

    public void changeWeight() {
        recorder.addEvent("looked at: ", "empty space", true);
        for (Entry<CuingOverlay, Double> e : weightMap.entrySet()) {
            CuingOverlay ct = e.getKey();
            weightMap.replace(ct, e.getValue() + ct.getWeightValue());
            //System.out.println("Weight: " + (current_weight + ct.getWeightValue()));
        }
        changeColors();
    }
}
