# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_version ">= 1.6.2"

Vagrant.configure("2") do |config|
    config.vm.define "vagrant-windows-81", autostart: false do |win8|
        win8.vm.box = "windows_81"
        win8.vm.communicator = "winrm"

        # Admin user name and password
        win8.winrm.username = "vagrant"
        win8.winrm.password = "vagrant"

        win8.vm.guest = :windows
        win8.windows.halt_timeout = 15

        win8.vm.network :forwarded_port, guest: 3389, host: 3389, id: "rdp",
            auto_correct: true
        win8.vm.network :forwarded_port, guest: 22, host: 2222, id: "ssh",
            auto_correct: true

        win8.vm.provision "shell", path: "vagrantprovision.ps1"

        win8.vm.synced_folder ".", "/cygdrive/c/vagrantsync", type: "rsync",
            rsync__exclude: [".git",'package.box','Session.vim','*.swp',
                'vagrantprovision.ps1','.vagrant'],
            rsync__args: ["--verbose", "--archive",
                "--delete",
                "-z",
                "--copy-links"]

        win8.vm.provider :virtualbox do |v, override|
            #v.gui = true (use `vagrant rdp` to connect to the GUI)
            v.customize ["modifyvm", :id, "--memory", 2048]
            v.customize ["modifyvm", :id, "--cpus", 2]
            v.customize ["setextradata", "global", "GUI/SuppressMessages", "all" ]
        end
    end

    config.vm.define "ubuntu", autostart: false do |ubuntu|
        ubuntu.vm.box = "ubuntu/xenial64"
        ubuntu.vm.synced_folder ".", "/vagrant", type: "rsync",
            rsync__exclude: [".git",'package.box','Session.vim','*.swp',
                'vagrantprovision.ps1','.vagrant'],
            rsync__args: ["--verbose", "--archive",
                "--delete",
                "-z",
                "--copy-links"]
        ubuntu.vm.provider :virtualbox do |v, override|
            v.gui = true
            v.customize ["modifyvm", :id, "--memory", 2048]
            v.customize ["modifyvm", :id, "--cpus", 2]
            v.customize ['modifyvm', :id, '--clipboard', 'bidirectional']
        end
        ubuntu.vm.provision "shell", inline: <<-SHELL
        apt-get update
        sudo apt-get install --no-install-recommends -y xfce4 lxdm
        sudo apt-get remove -y lxde-common
        sudo apt-get install -y virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11
        sudo usermod --password $(echo vagrant | openssl passwd -1 -stdin) ubuntu
        sudo echo 'allowed_users=anybody' > /etc/X11/Xwrapper.config
        sudo sed -i 's/Name=Xfce Session/Name=Xfce-Session/' /usr/share/xsessions/xfce.desktop
        apt-get install -y openjdk-8-jdk openjfx git
        SHELL
    end
end
