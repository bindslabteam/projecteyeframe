# Download A Build
1. go to [gitlab builds for
   the project](https://gitlab.com/bindslabteam/projecteyeframe/builds)
2. if a build passed, click the download artifacts button located on the far
   left.
   ![download build instructions](readmeResources/buildDownloadExample.png)
3. if a build has not passed and the error below is outputed in the console
   log, click the blue retry button to rebuild. Keep trying if the build fails
   again; it will eventually work. There is currently a glitch where
   connections are dropped.
   
   > $ apt-get update -qq && apt-get install -y -qq openjdk-8-jdk git
   >
   > E: Failed to fetch http://httpredir.debian.org/debian/pool/main/d/dbus-glib/libdbus-glib-1-2_0.102-1_amd64.deb  Error reading from server. Remote end closed connection [IP: 128.31.0.66 80]
   >
   > E: Failed to fetch http://httpredir.debian.org/debian/pool/main/x/x11proto-input/x11proto-input-dev_2.3.1-1_all.deb  Error reading from server. Remote end closed connection [IP: 128.31.0.66 80]
   >
   > E: Unable to fetch some archives, maybe run apt-get update or try with --fix-missing?

# Development
## get the code
### clone repo
`git clone --recursive <repository>`
### already cloned repo
`git submodule update --init --recursive`
## gradle
### run
`gradlew run`
### build
`gradlew build`
## eclipse
1. run `gradlew eclipse` to generate eclipse project files
2. open eclipse and load the folder as project


# Tools
- [gradle](https://gradle.org/gradle-download/)

# Libraries
- [jna](https://github.com/java-native-access/jna)
- [swt](http://www.eclipse.org/swt/)
- [Eclipse jface](http://wiki.eclipse.org/JFace#Deploying_a_Java_Program_That_Uses_SWT_and_JFace)
- [Eclipse ui forms](http://help.eclipse.org/luna/index.jsp?topic=%2Forg.eclipse.platform.doc.isv%2Fguide%2Fforms.htm)
- [jackson jr](https://github.com/FasterXML/jackson-jr)

## Depreciated
- [jna windowutils](https://github.com/Chrriis/DJ-Native-Swing)

# development software (not autoinstalled)
These currently require manual installation when using a vagrant instance.

Eclipse with Swing:
- [window builder](http://www.eclipse.org/windowbuilder/)

Eclipse With JavaFX:
- [scene builder](http://gluonhq.com/labs/scene-builder/)
- [eclipse javafx tools](http://www.eclipse.org/efxclipse/install.html#for-the-lazy)
- [tutorial for installing and using JavaFX](http://code.makery.ch/library/javafx-8-tutorial/part1/)

# API

The API uses sockets. Currently, it defaults to 127.0.0.1:4223. The address may
be changed in the Settings Menu.

The message format is big endian and starts with a 4 byte integer of the message
size which is followed by the message in the JSON format and encoded in utf-8.

JSON objects, which are have the notation "{}", are read in sequence, so order
matters. Such objects are refered to in other languages as "ordered dictionaries"
or "linked list hash maps". The ordering is important to ensure operations are
performed correctly. For example, to create an overlay and then move it; in
contrast, it would not make sense to the move an overlay that does not yet exist.

## Message Structure
Note that the structure of the message mimics calling functions in an OOP
language for simplicity.

The API format uses JSON and it structured as follows:

<blockquote>
[message size packed as a 4 byte integer]{
    "<Class Name>": {
        "<Object Name>": {
            "<Function Name>": [arg1, arg2, ...]
        },
        "<Static Function Name>": [arg1, arg2, ...],
        "<Static Function Name>": []
    }
}
</blockquote>

Note that "<Function Name>" will be called first, followed by the first "<Static Function Name>" and then the second "<Static Function Name>". The ordering of calls is guaranteed.

### Examples
<blockquote>
99{"SettingsPanel":{"switchToSettingsPanel":[]},"CuingOverlay":{"topLeftWindow":{"setSize":[10,20]}}}
</blockquote>

## Return Message Structure
Note that only functions that have return values are included. Error messages
are put into a seperate JSON object called "Error". If there are no return
values or error messages, an empty JSON object, "{}", will be returned.

### Examples
<blockquote>
[message size packed as a 4 byte integer]{
    "<Class Name>": {
        "<Object Name>": {
            "<Function Name>": <return value>
        },
    },
    "Error": {
        "<Class Name>": {
            "<Object Name>": {
                "<Function Name>": {
                    "Message": <error message for function>
                }
            },
        },
        "<Class Name>": {
            "<Object Name>": {
                "Message": <error message for object>
            },
        },
    }
}
</blockquote>

## Available API Calls
The list of available "<Class Name>" and "<Function Name>" that may be called
are available in the API calls Javadoc. Run `gradlew generateApiServerDocs` and
then open the "docs/api/calls/index.html" file in your web browser.


# Types of Groups
All groups are at minimum a superset of the regular weighted group.

### Regular (Only Weight)
overlays have weights

## Sequence Groups
sequence groups have overlays in an ordering. This ordering is prioritized
before the order generated by weights. Sequence groups do not have higher
priority than other sequence groups; they are alerted in the same order they
are queued.

### Rigid First Frame
when the first Overlay in the group is focused on, the strict sequence of the
group's overlays are pushed to the top of the stack, before the weighted
ordering, but after other sequence groups
The difference between "In Order" and "Out of Order", but requires that the
first overlay also be the most neglected overlay at the time it is focused on.
### Rigid Trigger Sequence
designate a number of the first overlays in the group to be a trigger sequence.
When each overlay in the trigger sequence is focused on in order, the strict sequence of the
group's overlays are pushed to the top of the stack, before the weighted
ordering, but after other sequence groups.

The difference between "In Order" and "Out of Order", but requires that the
trigger sequence of overlays also be the most neglected overlay at the time each overlay is focused on.

The difference between "Strict" and regular is that strict requires that the
trigger sequence of overlays be focused on in order with no other overlays
being focused on in between.
