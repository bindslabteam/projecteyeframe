# Echo client program
import socket
import struct

m = '{"SettingsPanel":{"switchToSettingsPanel":[]},"CuingOverlay":{"topLeftWindow":{"setSize":[10,20]}}}'
print 'Message Sent ({} bytes):'.format(len(m.encode("utf-8")))
print m

HOST = '127.0.0.1'    # The remote host
PORT = 4223              # The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.sendall(struct.pack('>i',len(m.encode("utf-8"))))
s.sendall(m)
data = s.recv(1024)
print ''
print ''
print 'Message Received:'
print repr(data.encode("utf-8"))
s.close()
