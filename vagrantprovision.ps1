iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
choco install -y cygwin cyg-get jdk8 gradle eclipse firefox
$Source = 'https://cygwin.com/setup-x86_64.exe'
$Destination = 'C:\tools\cygwin\cygwinsetup.exe'
Invoke-WebRequest -uri $Source -OutFile $Destination
Unblock-File $Destination
cyg-get install aspell autoconf automake bash-completion binutils cmake crypt csih ctags cygrunsrv cygwin-devel dejavu-fonts desktop-file-utils diffutils git git-completion gvim hexedit hicolor-icon-theme lua lyx make openssh patch patchutils perl pkg-config procps procps-ng python python-setuptools python3 python3-setuptools rsync ruby shared-mime-info texinfo tmux tzcode vim vim-common wget xxd
$Source = 'http://downloads.efxclipse.bestsolution.at/downloads/released/2.4.0/sdk/eclipse-SDK-4.6.0-win32-x86_64-distro-2.4.0.zip'
$Destination = 'C:\tools\eclipse-SDK-4.6.0-win32-x86_64-distro-2.4.0.zip'
Invoke-WebRequest -uri $Source -OutFile $Destination
# http://gluonhq.com/download/scene-builder-windows-x64/
$Source = 'http://download.gluonhq.com/scenebuilder/8.2.0/install/windows/SceneBuilder-8.2.0-x64.exe'
$Destination = 'C:\tools\SceneBuilder-x64.exe'
Invoke-WebRequest -uri $Source -OutFile $Destination
